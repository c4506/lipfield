#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 16:34:01 2021

@author: chevaugeon
"""

import numpy as np
import sys
sys.path.append('../lib')
import lip2d.lipdamage as lip2D
from lip2d.mesh import simplexMesh, dualLipMeshTriangle
import pylab as plt
import time
import cProfile



plt.close('all')
meshid ='2'
basemeshname = 'hole_ref'+meshid
meshfilename = '../msh/'+basemeshname+'.msh'

m = simplexMesh.readGMSH(meshfilename)
#m = dualLipMeshTriangle(m)
nv = m.nvertices
nf =  m.ntriangles

pr = cProfile.Profile()

def distance_to_line(x0, x1, x) :
    x01   = x1-x0
    alpha = x01.dot(x-x0)/x01.dot(x01)
    if (alpha >=0. and alpha <=1.) :
        return np.linalg.norm(x-x0-alpha*x01)
    elif alpha < 0.:
        return  np.linalg.norm(x-x0)
    else:
        return  np.linalg.norm(x-x1)

def distance_to_point(x0, x) :
    return np.linalg.norm(x0-x)
 
d = np.zeros(nv)    
d1 = np.zeros(nv)
d3 =  np.zeros(nv)
d2 = np.zeros(nv)
x0=np.array([-0.8,-0.75])
x1=np.array([0.8, -0.75])
x3 = np.array([0.8,0.8])
lc =0.05

#crackwidth = 0.5
#seeds = None
#for i, x in enumerate(m.xy) :
#   d[i] = max(0.,1. - distance_to_line(x0, x1, x)/crackwidth)
 
seeds = set()

for   i, x in enumerate(m.xy) :
    d1[i] = 0.3*np.cos(2.*x[0])*np.sin(2.*x[1])
    d2[i] = max(0., 1.-5*(distance_to_point(x3,x)))
    d3[i] = max(0., 1.-5*(distance_to_line(x0,x1,x)))
    d[i] = max(d1[i], d2[i], d3[i])
 
    
    
for v in range(nv):
        seeds.update([v])
    

#edge FM
def plot(d, lipmeasure, title):
    fig, (axd, axlip) = plt.subplots(1,2)
    c, f, a = m.plotScalarField(d, showmesh=False, fig =fig, ax = axd)
    c, f, a = m.plotScalarField(lipmeasure, showmesh=False, fig =fig, ax = axlip)
    axd.axis('equal')
    axlip.axis('equal')
    axd.set_title(title+ ' d ')
    axd.set_title(title+ r'$\|\nabla d\|$')
    f.colorbar(c, ax=a, orientation = 'vertical')


#plot targetok,
lip = lip2D.damageProjector2D(m)
ok, nglc = lip.islip(d, lc)
plot(d, nglc/lc, 'target, lc = %d'%lc)


side = 'up'

#edge FM
start = time.process_time()
res = lip.lipProjFM(d, lc, seeds =seeds, lipmeasure='edge', side = side, verbose = True)
end = time.process_time()
dup = res['d']
ok, nglc = lip.islip(dup, lc)
print('time edge FM lc2', end-start, 'max tlip measure ', max(nglc/lc), 'ok', ok)
plot(dup, nglc/lc, 'FM edge up, lc = %f'%lc)

#tri FM
start = time.process_time()
res = lip.lipProjFM(d, lc,  seeds =seeds, lipmeasure= 'triangle', side = side, verbose = True)
end = time.process_time()
dup = res['d']
ok, nglc = lip.islip(dup, lc)
print('time Triangle FM lc2', end-start, 'max tlip measure ', max(nglc/lc), 'ok', ok)
plot(dup, nglc/lc, 'FM triangle up, lc = %f'%lc)

#tri2 FM
start = time.process_time()
res = lip.lipProjFM(d, lc,  seeds =seeds, lipmeasure= 'triangle2', side =side, verbose = True)
end = time.process_time()
dup = res['d']
ok, nglc = lip.islip(dup, lc)
print('time Triangle FM v2 lc2', end-start, 'max tlip measure ', max(nglc/lc), 'ok', ok)
plot(dup, nglc/lc, 'FM v2 triangle up, lc = %f'%lc)

start = time.process_time()
res = lip.lipProjFM(d, lc,  seeds =seeds, lipmeasure= 'triangle3', side =side, verbose = True)
end = time.process_time()
dup = res['d']
ok, nglc = lip.islip(dup, lc)
print('time Triangle FM v3 lc2', end-start, 'max tlip measure ', max(nglc/lc), 'ok', ok)
plot(dup, nglc/lc, 'FM v3 triangle up, lc = %f'%lc)