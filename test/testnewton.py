#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Non Linear solver
  - newton

// Copyright (C) 2021 Chevaugeon Nicolas
Created on Fri Nov 12 12:42:17 2021
@author: nchevaug
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
sys.path.append('../.')
sys.path.append('../lib')


import numpy as np
import cvxopt
import nonlinearsolvers

def phi(x) :
    return x.dot(x) + x.dot(np.ones(x.shape))

def r(x) :
    return 2*x +np.ones(x.shape)

def dr(x):
    return cvxopt.spdiag([2.]*x.shape[0])
    
n = 2
x0 = np.zeros(n)
solver = nonlinearsolvers.newton( {'linsolve':'cholmod', 'itmax':1, 'maxnormabs':1.e-8, 'maxnormrel':1.e-6})

res = solver.solve(x0, r, dr)
print(res)
print(res['x'])

