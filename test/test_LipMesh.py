#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 16:18:59 2021

@author: chevaugeon
"""


import sys
sys.path.append('../lib')
import lip2d.mesh as mesh
import pylab as plt

plt.close('all')
meshid ='0'
basemeshname = 'hole_ref'+meshid
meshfilename = '../msh/'+basemeshname+'.msh'

# testfor Jihed's mesh
#meshid =''
#basemeshname = 'One_hole'+meshid
#meshfilename = '../msh/'+basemeshname+'.msh'

m = mesh.simplexMesh.readGMSH(meshfilename)
nv = m.nvertices
nf =  m.ntriangles
f, a = m.plot()
a.axis('equal')

lm = mesh.dualLipMeshTriangle(m)
lm.plot(f, a, color = 'r')
a.axis('equal')
a.set_axis_off()
f.savefig('lipmesh.pdf', format='pdf') 


# cogs = m.getTopCOG()
# lipmesh_triangle = triangle.triangulate({'vertices':cogs}, 'q30')
# tris = lipmesh_triangle['triangles']
# ver  = lipmesh_triangle['vertices']
# lm2= mesh.simplexMesh(ver, tris)
# f, a = lm2.plot()
# a.axis('equal')