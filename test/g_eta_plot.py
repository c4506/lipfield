#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Tue Aug 24 15:38:21 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""





import numpy as np
import scipy.integrate
import pylab as plt
import lip2d.material_laws_planestrain as laws
import lip2d.utils as utils

plt.matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})



plt.close('all')

E = 3500 #Mpa
nu = 0.32
KIc =  1.4 #Mpa sqrt(m)
Gc  = (1-nu**2)*KIc**2/E*1000#Mpa.mm
lc = 1.

#lc  = eperlc*hc  #~8*h near the crack
Yc  = Gc/4./lc#Mpa

lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
eta = 1./3.
h =  laws.HQuadratic()


fig, axs = plt.plt.subplots(1,1,  figsize = utils.set_size(300) )
axd = axs.twinx() 
#fig, axd = plt.plt.subplots(1,1,  figsize = utils.set_size(1000) )

for eta in[0.,0.1,0.2,0.3] :
    d = np.array([0.])
    geta =  laws.GO4Eta(eta)
    law = laws.SofteningElasticitySymetric(  lamb, mu, Yc = Yc, h =h, g = geta) 
    
    dh =[]
    s11=[]
    e11=[]
    for eps11 in np.linspace(0,10., 501) :
        eps11 *= np.sqrt(Yc / (0.5*lamb +  mu))
        eps22 = 0.
        eps12 = 0.
        strain = np.array([[eps11, eps22, 2*eps12]])
        d = law.solveSoftening(strain, np.array(d))
        stress = law.solveStressFixedSoftening(strain, d, deriv = False)[0]
        dh.append(d[0])
        e11.append(strain[0,0])
        s11.append(stress[0,0])
        
        
    ll = axs.plot(e11/np.sqrt(Yc / (0.5*lamb +  mu)), s11 / (2.*np.sqrt(Yc*(0.5*lamb + mu))), label = r'$\eta = $ %.2f'%eta)
    axd.plot(e11/np.sqrt(Yc / (0.5*lamb +  mu)), dh, color=ll[-1].get_color())
    
    ed = scipy.integrate.simps(s11,e11)
    print('energy dissipee ', ed)
    if eta != 0. :
        ef = np.sqrt(8*Yc/(mu+0.5*lamb)/eta)/np.sqrt(Yc / (0.5*lamb +  mu))
    else : ef = np.inf
    print('eps(d=1)', ef)

axs.set_xlabel(r"$\epsilon_{11}/\sqrt{2Y_c/(\lambda+2\mu)}$")
axs.set_ylabel(r"$\sigma_{11}/(2 \sqrt{Yc(\lambda + 2\mu)/2})$")
axd.set_ylabel(r"$d$")
axs.legend()
fig.tight_layout()
fig.savefig('g_eta_plt.pdf', format = 'pdf', bbox_inches='tight') 
fig.savefig('g_eta_plt.pgf', format = 'pgf', bbox_inches='tight')    
fig.savefig('g_eta_plt.svg', format = 'svg', bbox_inches='tight')    
fig.savefig('g_eta_plt.png', format = 'png', bbox_inches='tight')    
