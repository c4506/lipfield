#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 15:50:57 2021

@author: nchevaug
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

// Copyright (C) 2021 Chevaugeon Nicolas
Created on Tue May  4 10:46:36 2021
@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import sys
sys.path.append('../.')

import scipy
import numpy as np
import cvxopt
import linsolverinterface as lin
import liplog
import viscoelasticity_law2D as mat



class Mechanics2D:
    def __init__(self, mesh, law, lipprojector=None, lc=0.):
        self.mesh = mesh
        self.law = law
        self._strainop = None
        self._areas = None
        self._M = None
        
    def zeros(self):
        '''
        Returns u, eps1
        -------
        return a zeros np.array of shape (nvertices,2) for u and p.array of shape (ntriangles) for d.
        '''
        return np.zeros((self.mesh.nvertices,2)),  np.zeros((self.mesh.ntriangles,3))
        
    def strainOp(self) :
        ''' compute strain operator (B), such as  eps.flatten() = B * u.flatten() '''
        if self._strainop is None :    
            mesh =self.mesh
            nt = mesh.ntriangles
            nv = mesh.nvertices
            n = nt*12
            x = np.empty(n)
            I = np.empty(n, dtype ='int')
            J = np.empty(n, dtype ='int') 
            for it, t in enumerate(mesh.triangles) :
                 xy0 = xy = mesh.xy[t[0]]
                 xy = np.vstack ((mesh.xy[t[1]] - xy0, mesh.xy[t[2]] - xy0))
                 Be =  np.linalg.inv(xy).dot( np.array([[-1,1.,0.],[-1.,0.,1.]]))
                 Bt = list(Be[0,:]) + list(Be[1,:])*2+list(Be[0,:])
                 It =  [3*it]*3+[3*it+1]*3 +[3*it+2]*6
                 Jt =  (list(2*t)+list(2*t+1))*2
                 x[12*it:12*(it+1)] = Bt
                 I[12*it:12*(it+1)] = It
                 J[12*it:12*(it+1)] = Jt
            B = scipy.sparse.coo_matrix((x,(I,J)), shape =(3*nt, 2*nv) )
            self._strainop = B.tocsr()
        return self._strainop
    
    
    def strain(self, u):
        ushape = u.shape
        nv = self.mesh.nvertices
        u.reshape((nv,2))
        nf = self.mesh.ntriangles
        B = self.strainOp()
        strain = B.dot(u.reshape(2*nv)).reshape(nf,3)
        u.reshape(ushape)
        return strain
    
    def stress(self, u, eps1):
        """ return the stress in each element, as an array such as :
            stress[i,0] is sxx in element i
            stress[i,1] is syy in element i
            stress[i,2] is sxy in element i
            where 
            - u is an array shape (2*nv) where nv is the number of vertices in the mesh,
              such as u[2*i] is the displacement component in direction x of vertice,
              u[2*i+1] is the displacement component in direction y of vertice i
              d is an array of shape (ne) where ne is the number of element.
              such as d[i] is the damage variable in element i
        """
        strain = self.strain(u)
        return self.law.trialStress( strain, eps1)


    def areas(self): 
        """
        return the area of all triangles in the mesh

        Returns
        -------
        TYPE np.array, indexed by triangle id in the mesh
            areas[i] : area of triangle i.

        """
        if self._areas is None :
            mesh= self.mesh
            A = np.zeros(mesh.ntriangles)
            for it, t in enumerate(mesh.triangles) :
                xy0 = mesh.xy[t[0]]
                xy1 = mesh.xy[t[1]]
                xy2 = mesh.xy[t[2]]
                A[it] = np.cross(xy1-xy0, xy2-xy0)/2.
                self._areas = np.abs(A)      
        return self._areas
    
  
        
   
    
    def Dcvx(self, DT):
        nf = self.mesh.ntriangles
        H = self.law.dTrialStressDStrain(DT) 
        A = self.areas()
        return cvxopt.spdiag([ cvxopt.matrix(H[ie]*A[ie]) for ie in range(nf)])
    
    def F(self, stress): 
        ''' return the vecor of internal nodal forces'''
        Astress = self.areas()[:, np.newaxis]*stress
        F      = (self.strainOp().T).dot(Astress.reshape(nf*3))
        return F
    
    def Kcvx(self, DT):
        
       
        
        B = lin.convert2cvxoptSparse(self.strainOp())
        D = self.Dcvx(DT) 
        Kuu = B.T*(D*B)
        n = Kuu.size[0]
        
        nbnullpiv = 0
        kiimin =1.e9 
        for i, kii in enumerate(Kuu[::n+1]):
            kiimin = min(kii, kiimin)
            if (kii < 1.e-10) :
                Kuu[i,i] = 1.
                nbnullpiv += 1
        if nbnullpiv :   
           print('Warning ! mindiag ', kiimin, 'nullpiv ', nbnullpiv )
        return Kuu
    
    # def strainenergy(self,u ,  eps1) :
    #      nv = self.mesh.nvertices
    #      nf = self.mesh.ntriangles
    #      if u is None : u = np.zeros((nv, 2))
    #      if d is None : d = np.zeros((nf))
    #      strain = self.strain(u) 
    #      phi = self.law.potential(strain, d.squeeze())
    #      return self.integrate(phi)
         
     
    def integrate(self, elementfield):
        """ Given a constant field per element, integrate over the mesh """
        A = self.areas()
        if (elementfield.shape[0] != len(A)) :
            print('Error in integrate')
            raise
        return A.dot(elementfield)
            
    
            
    def solveDisplacementFixedDLinear(self, u, eps1, DT, imposed_displacements = None, imposed_nodal_forces = None, 
                                      solveroptions = {'linsolve':'cholmod'}, logger = liplog.getLogger()):
       
        eps = self.strain(u)
        stress, eps1 = self.law.solve_stress_eps1(self, eps, eps1, DT)
        
        F = self.F(stress)
        K = self.Kcvx(DT)
        n,n =K.size
        imposed = []
        free = list(range(0,n))
        nfree = len(free)
        nfixed = len(imposed)
        imposedval = cvxopt.matrix([], (nfixed,1))
        if imposed_displacements is not None :
                imposed =    list(imposed_displacements.keys())
                free = list(set(free).difference(set(imposed)))
                nfree = len(free)
                nfixed = len(imposed)
                imposedval = cvxopt.matrix(list(imposed_displacements.values()), (nfixed,1))
                Kff = K[free, free]
                Kfi = K[free,imposed]
                K = Kff
                F = -Kfi*imposedval
                
        res = lin.solve(K,-F, solver = solveroptions['linsolve'], solveroptions= solveroptions, logger=logger)
            
        if res['Converged'] :
            x = res['x']
            u=cvxopt.matrix(0., (n,1))
                   
            u[free] =    x[:nfree,0]
            lagmul = +x[nfree:]
            u[imposed] = imposedval[:,0]
            R = np.array(K*(u)).squeeze()
            u  = np.array(u).reshape(n//2, 2)
            res ={'u':u, 'eps1':eps1, 'R': R, 'lagmul':lagmul, 'Converged':True}
            return res
        
            
        logger.error('Linear Solver failed')
        raise
            
   