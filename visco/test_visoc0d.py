#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 15:51:50 2021

@author: nchevaug
"""
import sys
sys.path.append('../.')
import viscoelasticity_law2D as visclaw
import pylab as plt

plt.close('all')   
 
E0=1.
E1=0.5 
nu = 0.3
tau = 0.1
lamb0, mu0 = (E0*nu/(1.+nu)/(1.-2*nu), E0/2./(1+nu))

lamb1, mu1 = (E1*nu/(1.+nu)/(1.-2*nu), E1/2./(1+nu))


law = visclaw.viscoElasticity2dPlaneStrain(lamb0, mu0, lamb1, mu1, tau )

epsxxend = 1.
DT = 0.001

for epsdot in [10**n for n in [ -1, 0, 1,2, 3 ]] :
    print( 'epsdot', epsdot)
    eps, stress, T = visclaw.solve0d(law, epsdot, epsxxend, DT)
    plt.plot(eps, stress, label=str(epsdot))

plt.legend()