#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 14:23:27 2021

@author: nchevaug
"""

import numpy as np
import pylab as plt

class viscoElasticity2dPlaneStrain():
    def __init__(self, lamb0 = 1., mu0 =1., lamb1=1., mu1=1., tau = 0.1):
        self.lamb0 = lamb0
        self.mu0 = mu0
        
        self.lamb1 = lamb1
        self.mu1= mu1
        
        self.tau = tau
        
        self.H0 =  np.array( [[lamb0+2*mu0, lamb0, 0.],
                              [lamb0, lamb0+2*mu0, 0.],
                              [0.,0., mu0]])
        
        self.H1 =  np.array( [[lamb1+2*mu1, lamb1, 0.],
                              [lamb1, lamb1+2*mu1, 0.],
                              [0.,0., mu1]])
        self.invH1 = np.linalg.inv(self.H1)
        
    def nu(self):
        mu = self.mu0
        lamb = self.lamb0
        return lamb/(2*(lamb+mu)) 
    
    def solve_stress_eps1(self, eps, eps1, DT):
        if len(eps.shape) == 1 :
            print('qsdfsdqfsdqfqsdf')
            ne = 1
            if eps.shape[0] != 3 : raise
            eps = eps.reshape(1,3)
            eps1 = eps1.reshape(1,3)
            
        else :
            print('AAAAAAAAAAAAAAA')
            
            ne = eps.shape[0]
            if eps.shape[1] !=3 : raise
            
        if eps.shape != eps1.shape : raise
        
        tau =self.tau
        invH1 = self.invH1
        H = self.dTrialStressDStrain(eps, DT)
        if len(H.shape) != 3 : H = H.reshape(1,3,3)
        
        stresses = np.zeros( (ne, 3) )
        
        print('ggggg')
        print(eps)
        print(eps1)
        
        
        for i, (epse, eps1e) in enumerate(zip(eps, eps1)) :  
            
            print(epse)
            print(eps1e)
            print(H[i])
            stresse = np.dot(H[i], (epse - tau/(DT+tau) *eps1e))
            print('stress', stresse)
            
            print(np.dot(invH1,stresse))
            eps1e = 1./(DT+tau) * (np.dot(invH1,stresse.T).T*DT+ eps1e*tau) 
            stresses[i] = stresse
            eps1[i]    = eps1e 
    
        return stresses.squeeze(), eps1.squeeze()
    
    def dTrialStressDStrain(self, eps, DT):
        ''' return a array of array . ret[i] is the hook tensor for element i ... 
        Note : not very usefull for now since the H is the same for elements'''
        
        if len(eps.shape) == 1 :
            ne = 1
            if eps.shape[0] != 3 : raise
            eps.reshape(1,3)
            
        else :
            ne = eps.shape[0]
            if eps.shape[1] !=3 : raise
        
        
        tau =self.tau
        H0 = self.H0
        invH1 = self.invH1
        H0 = self.H0
        
        Htmp = np.eye(3) + DT/(DT + tau)*np.dot(H0,invH1)
        H= np.dot(np.linalg.inv(Htmp), H0)
        ne= eps.shape[0]
        return np.tensordot( np.ones(ne) ,  H, axes = 0 ).squeeze()
    
    def trialStress(self, strain, eps1):
        return np.dot(self.H0, strain-eps1)



def solve0d(law, epsdot, epsxxend, DT)  :
    eps1 = np.array([0.,0.,0.])
    T = 0.
    epsxx = 0.
    Ttab =[0.]
    stresstab=[ np.array([0.,0.,0.])]
    epstab = [np.array([0.,0.,0.])]
    eps1tab =[np.array([0.,0.,0.])]
    nu = law.nu()
    while epsxx < epsxxend:
        T+=DT
        epsxx += epsdot*DT
        eps = np.array([epsxx, -nu*epsxx, 0. ])
        
        stress, eps1 = law.solve_stress_eps1(eps, eps1, DT) 
        epstab.append(eps)
        eps1tab.append(eps1)
        stresstab.append(stress)
        Ttab.append(T)
        print('   T', T)
       
    epsxx = np.array( [ eps[0] for eps in epstab ] )  
    stressxx = np.array( [ stress[0] for stress in stresstab ] )  
    return epsxx, stressxx, np.array(T)
    


 #   def dstressdeps(self, eps, eps1, DT):
        