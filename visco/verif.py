#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 16:23:08 2021

@author: nchevaug
"""

import sys
sys.path.append('../.')

import viscoelasticity_law2D as visclaw
import viscoelasticymech2d   as mech2d
from mesh import simplexMesh
import liplog
import numpy as np

E0=1.
E1=0.5 
nu = 0.3
tau = 0.1
lamb0, mu0 = (E0*nu/(1.+nu)/(1.-2*nu), E0/2./(1+nu))

lamb1, mu1 = (E1*nu/(1.+nu)/(1.-2*nu), E1/2./(1+nu))


law = visclaw.viscoElasticity2dPlaneStrain(lamb0, mu0, lamb1, mu1, tau )

basemeshname = 'hole'

meshfilename = '../msh/'+basemeshname+'.msh'

mesh = simplexMesh.readGMSH(meshfilename)
nv = mesh.nvertices
nf =  mesh.ntriangles


logger = liplog.getLogger()
logger.info('Mesh Loaded from file ' + meshfilename)
logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))



mech = mech2d.Mechanics2D(mesh, law)    

u, eps1 = mech.zeros()   
speed = 1.
DT = 0.01
     

idl0 = 10#id of fixed line
idl1 = 12#id of pulled line
vidl0  = mesh.getVerticesOnClassifiedEdges(idl0) #ids of the vertices on fixed line
vidl1  = mesh.getVerticesOnClassifiedEdges(idl1) #ids of the vertices on pulled line

imposed_displacement= dict()

# Dirichlet Boundary conditions. degree of freedom are indexed by vertice Id  : ux(id) -> u(id*2), uy(id)   = u(8d*2+1)
for vid in vidl0:
    imposed_displacement[2*int(vid)] = 0.
    imposed_displacement[2*int(vid)+1] = 0.

for vid in  vidl1:
    imposed_displacement[2*int(vid)] =   speed*DT     
    imposed_displacement[2*int(vid)+1] = 0.    
    
    
# res = mech.solveDisplacementFixedDLinear(u, eps1, DT, imposed_displacement = imposed_displacement)

# u = res['u']
# eps1 = res['eps1']

# strain = mech.
# stress = law
    
eps = np.array( [[1.,0.1,0.1], [0.5,0.,0.]])
eps1 = np.array( [[1.,0.1,0.1], [0.5,0.,0.]])

stress, eps1 = law.solve_stress_eps1(eps, eps1, DT) 