from setuptools import setup
setup(name='lip2d',
      version='0.1',
      description='A finite Element Library for the lipfield method',
      #url='http://ictshore.com/',
      author='Chevaugeon Nicolas',
      author_email='nicolas.chevaugeon@ec-nantes.fr',
      license='GNU GPL',
      packages=['lip2d'],
      zip_safe=False)
