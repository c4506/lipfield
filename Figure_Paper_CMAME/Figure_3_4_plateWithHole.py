#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#Solve lipfield for a 2d square plate with hole in plane strain.
#Parameter are same as in lip paper 2. Produce figure 3 of the paper.

import os
import matplotlib.pyplot as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from   lip2d.mesh import simplexMesh, dualLipMeshTriangle
import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector
import lip2d.liplog    as liplog
#import datetime
import shutil
import pathlib
import pickle


def compute(logger, meshid =1, respath = pathlib.Path("")):
    respath = respath/ pathlib.Path('mesh_'+str(meshid))
    meshfilename  = 'msh/hole_'+str(meshid)+'.msh'
    lc = 0.2 # lenght scaleof the lip constrain
    E = 1.
    nu = 0.3
    eta = 0.1
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    onlineplot = False
    plotpatches = False
    plt.close('all')
    os.makedirs(respath, exist_ok = False)
    shutil.copy(__file__, respath / "Run_script.py")
    logger.info('Starting Program plateWithHole')
    logger.info('All results saved in ' + str(respath))
    
    savefilename = respath / "fields"
   
    fieldsave  = liplog.stepArraysFiles(savefilename)
    
    law = mlaws.SofteningElasticitySymmetric(lamb, mu,
                                            h = mlaws.HQuadratic(), 
                                            g = mlaws.GO4Eta(eta))
    solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
    solverdispoptions = {'linsolve':'cholmod'}
    
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
    
    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        }
    
    alternedsolveroptions= {'abstole':1.e-8, 
                            'reltole':1.e-5,  
                            'deltadtol':1.e-5, 
                            'outputalliter':False, 
                            'verbose':False, 
                            'stoponfailure':False}
    alternedsolveroptions= {'abstole':1.e-7, 
                            'reltole':1.e-4,  
                            'deltadtol':1.e-4, 
                            'outputalliter':False, 
                            'verbose':False, 
                            'stoponfailure':False}
    
    mesh = simplexMesh.readGMSH(meshfilename)
    nv = mesh.nvertices
    nf =  mesh.ntriangles
    logger.info('Mesh Loaded from file ' + meshfilename)
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    
    lipproj = lipprojector.damageProjector2D(lipmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
         
    idfix  = 10#id of fixed line
    idpull = 12#id of pulled line
    
    
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([idfix],{'x':0., 'y':0.})
    bc.dirichlets.add([idpull],{'y':0.})
    did = bc.dirichlets.add([idpull],{'x':0.})
    
    timer = liplog.timer()   

    with open(respath / "model", "wb") as filehandler :
        pickle.dump(mech,filehandler)
              
    
    u= np.zeros((nv, 2))
    d = np.zeros((mesh.ntriangles))
    
    uimp=[]
    Fx  =[]
    etab= []
    showmesh = nv < 1000
    fig0, ax0 = plt.subplots(1,1)
    fig0.suptitle(r'$F(u)$')
    
    ax0.set_xlabel(r'Imposed Displacement')
    ax0.set_ylabel(r'Reaction Force')
    
    
    # load increment (displacement of the right boundary)
    incs = [0.,1., 1.25, 1.5, 1.6, 1.7,1.8,1.9,2.,2.1, 2.3,2.5,2.7,2.75,2.8,3., 3.5, 4., 4.5, 5.] 
    for step, ui in enumerate(incs) : 
        timer.new_increment()
        timer.start('total')
        logger.info('#Start uimp = '+ str(ui))
           
        bc.dirichlets.update(did,{'x':ui})
            
        dmin = d.copy() 
        res = mech.alternedDispDSolver(dmin = dmin, dguess= d.copy(), un=u, 
                                       bc = bc,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions,
                                       timer =timer)
        dtmp1 = res['d']
        dmind1 = np.linalg.norm(dtmp1-dmin)
        logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dmin-d1|= ' + '%2.e'%dmind1)
        u = res['u']
        d = res['d']
        timer.start('post')
        if onlineplot :     mech.plots(u, d, ui, savefilename, showmesh = showmesh) 
        if plotpatches :    mech.plotPatches(res, savefilename, ui)
            
        timer.end('post')
        if not (res['Converged']) : raise
        
        R = res['R']
        strain = mech.strain(u)
        e = mech.energy(strain, d)
    
    
        Rx = sum(bc.dirichlets.getReactions(did, R))
        
        uimp.append(ui)
        etab.append(e)
        Fx.append(Rx)
        fieldsave.save(step,  u = u, d = d)
            
        ax0.plot(np.array(uimp), np.array(Fx), '-o')
        timer.end('total')
        timer.log(logger)
    
   
    
        
    uimp = np.array(uimp)
    with open( respath / 'loadDisplacement.npy','wb') as f :
        np.save(f,uimp)
        np.save(f,Fx)
        f.close()
    
    if onlineplot:
        fig, ax = plt.subplots(1,1)
        fig.suptitle(r'$F(u)$')
        plt.plot(uimp, np.array(Fx), '-o')
        ax.set_xlabel(r'Imposed Displacement')
        ax.set_ylabel(r'Reaction Force')
        
        fig, ax = plt.subplots(1,1)
        fig.suptitle(r'$e(u)$')
        plt.plot(uimp, np.array(etab), '-o')
        ax.set_xlabel(r'Imposed Displacement')
        ax.set_ylabel(r'energy')
   
    return respath
    
def Figure3( respathbase):
    import FiguresCommon
    fig, axcurve, axesd, caxbar = FiguresCommon.createFigAxesCurve5Map()
    
    respath = respathbase/pathlib.Path('mesh_1')
    with open(respath / "model", "rb") as filehandler :
        mech = pickle.load(filehandler) 
    loadfilename = respath / "fields"
    fieldload = liplog.stepArraysFiles(loadfilename)
    
    #fig, axes = plt.subplots(2, 3, figsize=(12,12/3*2), squeeze = True)
    
    with open(respath/'loadDisplacement.npy', 'rb') as f:
        imposed_disp  = np.load(f)
        reaction_x    = np.load(f)
        
    axcurve.set_xlabel(r'Imposed Displacement $[mm]$')
    axcurve.set_ylabel(r'Reaction Force $[N/mm]$')
    axcurve.plot(imposed_disp, reaction_x,'-')
    
    axcurve.set_xlim(-0.2,2.2)
    def myplotstep(step, fig, ax):
        rstep =  fieldload.load(step)
        #ux = incs[step]
        u  = rstep['u']
        d  = rstep['d']
        #stress = mech.stress(u, d)
        color, figd, axd  = mech.mesh.plotScalarField(d, u*0.,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = ax)
       
        axd.set_axis_off()
        axd.axis('equal')
        #axd.set_title(r"$D$") 
        axcurve.plot(imposed_disp[step], reaction_x[step],'o',color='blue')
        return color

    for step, ax in zip([1,3,5,6,7], axesd) :
        color= myplotstep(step, fig, ax)
    
    cbard = fig.colorbar(color, cax= caxbar, orientation='horizontal')
    cbard.set_label(r'$d$')
    cbard.set_ticks([0.,0.5,1.])
    
    return fig
    

    
def Figure4(respathbase):
    step = 18
    #fig, axes = plt.subplots(3, 3, figsize=(12,12), squeeze = True)
    
    a = 2
    b = a*15
    fig = plt.figure(figsize=(12,(12./3)*(3.+3*a/b)))
   
    axmap0  = fig.add_subplot(3*b+3*a,3, (1,1+3*(b-1)))
    axmap1  = fig.add_subplot(3*b+3*a,3, (2,2+3*(b-1)))
    axmap2  = fig.add_subplot(3*b+3*a,3, (3,3+3*(b-1)))
    caxbar  = fig.add_subplot(3*b+3*a,3, (2+3*(b),2+3*b))
    axdcut0 = fig.add_subplot(3*b+3*a,3, (1+3*b+6*a+3, 1+3*b+6*a+3+3*(b-1)-9))
    axdcut1 = fig.add_subplot(3*b+3*a,3, (2+3*b+6*a+3, 2+3*b+6*a+3+3*(b-1)-9))
    axdcut2 = fig.add_subplot(3*b+3*a,3, (3+3*b+6*a+3, 3+3*b+6*a+3+3*(b-1)-9))
    axucut0 = fig.add_subplot(3*b+3*a,3, (1+6*b+6*a+6, 1+6*b+6*a+6+3*(b-1)-9))
    axucut1 = fig.add_subplot(3*b+3*a,3, (2+6*b+6*a+6, 2+6*b+6*a+6+3*(b-1)-9))
    axucut2 = fig.add_subplot(3*b+3*a,3, (3+6*b+6*a+6, 3+6*b+6*a+6+3*(b-1)-9))
    
    axesmap = [axmap0, axmap1, axmap2]
    axesdcut = [axdcut0, axdcut1, axdcut2]
    axesucut = [axucut0, axucut1, axucut2]

    
    cutline = np.array([[-0.4, 0.5],[0.4,0.5]])
    cutlinenpts = 10000
    for m in [1,2,3]:
        #logger.info('Plotting Step %d'%step)
        respath = respathbase/pathlib.Path('mesh_'+str(m))
        with open(respath / "model", "rb") as filehandler :
            mech = pickle.load(filehandler)
        mesh = mech.mesh
        lmesh = mech.lipprojector.mesh
        loadfilename = respath / "fields"
        fieldload = liplog.stepArraysFiles(loadfilename)
        #logger.info('Plotting Step %d'%step)
        rstep =  fieldload.load(step)
        #ux = incs[step]
        u  = rstep['u']
        d  = rstep['d']
        #stress = mech.stress(u, d)
        cd, figd, axd  = mesh.plotScalarField(d, u*0.,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = axesmap[m-1])
        axd.set_axis_off()
        axd.axis('equal')

        axd.plot(cutline[:,0], cutline[:,1], color =  'w', linewidth =1 )
        axlined = axesdcut[ m-1]
        resm =  mesh.extractScalarElemFieldOnLine( d, cutline[0], cutline[1], cutlinenpts)
        reslm = lmesh.extractScalarVertexFieldOnLine( d, cutline[0], cutline[1], cutlinenpts)
        axlined.plot(resm['x'],  resm['v'],  color = 'tab:blue', label = 'extracted from mesh')
        axlined.plot(reslm['x'], reslm['v'], color = 'tab:red', label = 'extracted from lip-mesh')
        axlined.set_xlabel(r'x')
        axlined.set_ylabel(r'd')
        if (m == 3):
            axlined.legend(loc = 'upper center')
            
        axlineu  = axesucut[m-1]
        coloru   = 'tab:blue'
        axlineu.set_xlabel(r'x')
        if (m==1) :
            axlineu.set_ylabel(r'$u_x$', color=coloru)  
        axlineu.tick_params(axis='y', labelcolor=coloru)
        resu =  mesh.extractScalarVertexFieldOnLine( u[:,0], cutline[0], cutline[1], cutlinenpts)
        axlineu.plot(resu['x'], resu['v'], coloru)
        axlineeps = axlineu.twinx()
        coloreps =  'tab:red'
        if (m==3) :
            axlineeps.set_ylabel(r'$\epsilon_{xx}$', color=coloreps)  
        axlineeps.tick_params(axis='y', labelcolor=coloreps)
        reseps =  mesh.extractScalarElemFieldOnLine( mech.strain(u)[:,0], cutline[0], cutline[1], cutlinenpts)
        axlineeps.plot(reseps['x'], reseps['v'], coloreps)
        
    cbard = fig.colorbar(cd, cax= caxbar, orientation='horizontal')
    cbard.set_label(r'$d$')
    cbard.set_ticks([0.,0.5,1.])
    
    return fig
    
 
def plot(respath, figpath):
    ''' Plot the figure 3 and 4 of CMAME paper : plate with a Hole'''  
    fig3 = Figure3(respath)
    fig3.savefig(figpath/"Fig_3_PlateWithHole_a.png", format = 'png', dpi =400, bbox_inches='tight')
    fig3.savefig(figpath/"Fig_3_PlateWithHole_a.pdf", format = 'pdf', bbox_inches='tight')
    
    fig4 = Figure4(respath)
    fig4.savefig(figpath/"Fig_4_PlateWithHole_b.png", format = 'png', dpi =400, bbox_inches='tight')
    fig4.savefig(figpath/"Fig_4_PlateWithHole_b.pdf", format = 'pdf', bbox_inches='tight')
    
if __name__ == '__main__':
    import sys
    import pathlib
    docomp = True
   
    respath = pathlib.Path.home()/pathlib.Path('tmp/resultsPlateWithHole')
    figpath = pathlib.Path('figures')
    if len(sys.argv )> 1:
        #now = datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")
        respath = pathlib.Path(sys.argv[1])
    if len(sys.argv )> 2:
        figpath =  pathlib.Path(sys.argv[2])
    if docomp  : 
        os.makedirs(respath, exist_ok = False)
        logger = liplog.setLogger(respath/"plateWithHole.log") 
        compute(logger, 1, respath)
        compute(logger, 2, respath)
        compute(logger, 3, respath)
    
    plot(respath, figpath)
