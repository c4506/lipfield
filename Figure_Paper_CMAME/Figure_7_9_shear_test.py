#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""



import os
import matplotlib.pylab as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from lip2d.mesh import simplexMesh, dualLipMeshTriangle

import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector
import lip2d.liplog as liplog
#import datetime
import shutil
import pathlib
import pickle


def compute(respath, assymlaw):
    onlineplot = True
    lc = 0.015 # lenght scaleof the lip constrain
    Gc = 2.7 # 2700 J/m2 -> 2.7 Mpa.mm
    E = 210000 #MPa
    nu = 0.2
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    Yc = Gc/4./lc
    eta = 0.1
    
    if assymlaw :
        basemeshname = 'shear_test_assym'
        law = mlaws.SofteningElasticityAsymmetric(lamb, mu, Yc,
                                                        h = mlaws.HQuadratic(), 
                                                        g = mlaws.GO4Eta(eta))
        # Solver for equilibrium, need nonlinear solver (newton)
        solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDNonLinear
        solverdispoptions =  {'linsolve':'cholmod', 'itmax':20, 'maxnormabs':1.e-8, 'maxnormrel':1.e-6}
        umax = 0.03
    else :
        basemeshname = 'shear_test'
        # Define the material law 
        law = mlaws.SofteningElasticitySymmetric(lamb, mu, Yc,
                                                        h = mlaws.HQuadratic(), 
                                                        g = mlaws.GO4Eta(eta))
        # Solver for equilibrium
        solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
        solverdispoptions = {'linsolve':'cholmod'}
        umax = 0.05
        
    incs = np.linspace(0.,umax,401)
    os.makedirs(respath, exist_ok = False)
    shutil.copy(__file__, respath/'Run_script.py')
    logger = liplog.setLogger(respath/'ShearTest.log')     
    savefilepath =  respath/ 'fields'
    fieldsave  = liplog.stepArraysFiles(savefilepath)   
    logger.info('Starting Program shear_test, results saved in '+ str(respath.absolute()))
   
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        }
    alternedsolveroptions= {'abstole':1.e-8, 'reltole':1.e-5,  'deltadtol':1.e-4, 'outputalliter':True, 'verbose':False, 'stoponfailure':False}
    meshfilepath = pathlib.Path('msh/'+basemeshname+'.msh')
    mesh = simplexMesh.readGMSH(meshfilepath)
    nv = mesh.nvertices
    nf =  mesh.ntriangles
    logger.info('Mesh Loaded from file ' + str(meshfilepath.absolute()))
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    lipproj = lipprojector.damageProjector2D(lipmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
    fixedlineid = 100#id of fixed line
    pulledlineid = 101#id of pulled line
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([fixedlineid],{'x':0., 'y':0.})
    bc.dirichlets.add([pulledlineid],{'y':0.})
    did = bc.dirichlets.add([pulledlineid],{'x':0.})
        
    u, d= mech.zeros()  
    uimp=[]
    Fx  =[]
    etab= []
    showmesh = nv < 1000
    
    with open(respath / "model", "wb") as filehandler :
        pickle.dump(mech,filehandler)

    ### global timing info
    timer = liplog.timer() 
    for step, ui in enumerate(incs) : 
        logger.info('#Start uimp = '+ str(ui))
        timer.new_increment()
        timer.start('total')     
        bc.dirichlets.update(did,{'x':ui})
        dn = d.copy() 
        res = mech.alternedDispDSolver(dmin = dn, dguess= d.copy(), un=u, 
                                       bc=bc, 
                                       incstr='Step %04d'%step,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions,
                                       timer =timer)
        u, d, R = res['u'], res['d'], res['R']
        fieldsave.save(step,  u = u, d = d, R = R) 
        logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dn-d|= ' + '%2.e'%np.linalg.norm(d-dn))
        if onlineplot :     mech.plots(u, d, ui, respath/'plot', fields =np.array([['d_1','stressxy' ],['Y', 'd']]),  showmesh = showmesh) 
        if not (res['Converged']) : raise
        
        strain =mech.strain(u)
        e = mech.energy(strain, d)   
        Rx = sum(bc.dirichlets.getReactions(did, R))
        uimp.append(ui)
        etab.append(e)
        Fx.append(Rx)
        
        timer.end('total')
        timer.log(logger)
       

    f =  open(respath/'RU.npy', 'wb')
    np.save(f,np.array(uimp))
    np.save(f,np.array(Fx))
    f.close()
    
    fig, ax = plt.subplots(1,1)
    fig.suptitle(r'$F(u)$')
    plt.plot(uimp, np.array(Fx), '-o')
    ax.set_xlabel(r'Imposed Displacement')
    ax.set_ylabel(r'Reaction Force')
    fig.savefig(respath/'Load_Displacement.pdf', format = 'pdf')    
    
    
def Figure(respath,  steps, uscale = 0.):
    import FiguresCommon
    fig, axcurve, axesd, caxbar = FiguresCommon.createFigAxesCurve5Map()
    
    with open(respath / "model", "rb") as filehandler :
        mech = pickle.load(filehandler)

    with open(respath/'RU.npy', 'rb')  as ru :
       ui = np.load(ru)
       ri = np.load(ru)
       
    axcurve.set_xlabel(r'Imposed displacement in $x$ direction $[mm]$')
    axcurve.set_ylabel(r'Reaction Force in $x$ direction $[N/mm]$')  # we already handled the x-label with ax1
    axcurve.plot(ui,ri, color='blue')

    for ax, step in zip(axesd, steps) :
        d = liplog.stepArraysFiles(respath/"fields").load(step)['d']
        u = liplog.stepArraysFiles(respath/"fields").load(step)['u']
        # c, fig, ax  = mech.mesh.plotScalarField(d,u*10, Tmin = 0., Tmax =0.99,  fig = fig, ax = ax, showmesh=False)
        c, fig, ax  = mech.mesh.plotScalarElemField( d, u*uscale, Tmin = 0., Tmax =1.,  fig = fig, ax = ax, showmesh=False,  eraseifoutofbound = False)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
       
        ax.axis('equal')
        ax.set_axis_off()
        axcurve.plot(ui[step],ri[step], 'o', color='blue')
   
    cbard = fig.colorbar(c, cax= caxbar, orientation='horizontal')
    cbard.set_label(r'$d$')
    cbard.set_ticks([0.,0.5,1.])
    return fig
    
def plot(respathsym, respathasym, figpath):
    import os
    os.makedirs(figpath, exist_ok = True)
    fig8 = Figure(respathsym,   [70, 78, 100, 150, 400], uscale = 0.)
    fig9 = Figure(respathasym,  [121, 129, 153, 250, 350], uscale = 0.)
    
    fig8.savefig(figpath/"Fig_8_ShearTest.png", format = 'png', dpi =400, bbox_inches='tight')
    fig8.savefig(figpath/"Fig_8_ShearTest.pdf", format = 'pdf', bbox_inches='tight')
    
   
    fig9.savefig(figpath/"Fig_9_ShearTest.png", format = 'png', dpi =400, bbox_inches='tight')
    fig9.savefig(figpath/"Fig_9_ShearTest.pdf", format = 'pdf', bbox_inches='tight')

if __name__ == '__main__':
    import sys
    
    run  = True
    plt.close('all')
    figpath = pathlib.Path('figures')
    respath = pathlib.Path.home()/pathlib.Path('tmp/resultsShearTest')
    
    if len(sys.argv )> 1:  respath = pathlib.Path(sys.argv[1]) 
        
    respathasym = respath/pathlib.Path('Asym')
    respathsym = respath/pathlib.Path('Sym')
    
    if run :
        compute( respathsym, False)
        compute( respathasym, True)
         
    plot(respathsym, respathasym, figpath)
    
    #post(respath)
