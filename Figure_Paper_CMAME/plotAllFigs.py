#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 22:48:29 2022

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""



''' plot all results figures for paper CMAME, assuming that the computation have been done and saved in respath '''
import pathlib
import os
figpath = pathlib.Path('figures')
os.makedirs(figpath, exist_ok = True)

print('Figure 1')
import Figure_1
Figure_1.plot(figpath)

print('Figure 2')
import Figure_2_LipMesh
Figure_2_LipMesh.plot(figpath)

print('Figure 3, 4')
import Figure_3_4_plateWithHole
respath = pathlib.Path.home()/pathlib.Path('tmp/resultsPlateWithHole')
Figure_3_4_plateWithHole.plot(respath, figpath)

print('Figure 6')
import Figure_6_tdcb
respath = pathlib.Path.home()/pathlib.Path('tmp/resultsTDCB')
Figure_6_tdcb.plot(respath, figpath)

print('Figure 7 9')
import Figure_7_9_shear_test
respath = pathlib.Path.home()/pathlib.Path('tmp/resultsShearTest')
respathasym = respath/pathlib.Path('Asym')
respathsym = respath/pathlib.Path('Sym')
Figure_7_9_shear_test.plot(respathsym, respathasym, figpath)

print('Figure 11')
import Figure_11_TwoEdgeCracks
respath = pathlib.Path.home()/pathlib.Path('tmp/resultsTwoEdgesCrack')
Figure_11_TwoEdgeCracks.plot(respath, figpath)

print('Figure 12')
import Figure_12_Projections
Figure_12_Projections.plot(figpath)


