#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Tue Aug 24 15:38:21 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pathlib

def plot(figpath):
    ''' plot an example of a mesh and associated lipmesh. Figure 2 of CMAME paper'''
    import lip2d.mesh as mesh
    import pylab as plt
    import os
    os.makedirs(figpath, exist_ok = True)
    meshfilename = 'msh/hole_0.msh'
    m = mesh.simplexMesh.readGMSH(meshfilename)
    f = plt.figure()
    f.set_size_inches(12,12)
    a = plt.Axes(f, [0., 0., 1., 1.])
    a.set_axis_off()
    f.add_axes(a)
    f, a = m.plot(f, a)
    lm = mesh.dualLipMeshTriangle(m)
    lm.plot(f, a, color = 'r')
    a.axis('equal')
    a.set_axis_off()
    f.savefig(figpath/'Fig_2_lipmesh.png', format='png', bbox_inches=0) 
    f.savefig(figpath/'Fig_2_lipmesh.pdf', format='pdf', bbox_inches=0)
    
if __name__ == '__main__':
    figpath = pathlib.Path('figures')
    plot(figpath)
