#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 01:32:04 2022

@author: nchevaug
"""
import pylab as plt

def  createFigAxesCurve5Map():
    a = 2
    b = a*15
    fig = plt.figure(figsize=(12,(12./3)*(2.+a/b)))
    axcurve = fig.add_subplot(2*b+a,3,  (1+3*3,1+3*(b-4)))
    axed0 = fig.add_subplot(2*b+a,3,  (2,2+3*(b-1)))
    axed1 = fig.add_subplot(2*b+a,3,  (3,3+3*(b-1)))
    
    axed2 = fig.add_subplot(2*b+a,3,  (1+3*(b),1+3*(2*b-1)))
    axed3 = fig.add_subplot(2*b+a,3,  (2+3*(b),2+3*(2*b-1)))
    axed4 = fig.add_subplot(2*b+a,3,  (3+3*(b),3+3*(2*b-1)))
    axbar = fig.add_subplot(2*b+a,3,  (2+6*(b),2+6*(b)))
    axesd = [axed0,axed1, axed2,axed3,axed4]
    return fig, axcurve, axesd, axbar