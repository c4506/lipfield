#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
sys.path = ['/home/chevaugeon/.local/lib/python3.8/site-packages/cvxopt-1.2.7+0.gd5a21cf.dirty-py3.8-linux-x86_64.egg'] + sys.path
#import cProfile
import shutil
import os
import pathlib
import pickle
import matplotlib.pylab as plt
import numpy as np

import lip2d.material_laws_planestrain as mlaws
import lip2d.liplog as liplog
from lip2d.mesh import simplexMesh, dualLipMeshTriangle
import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector

''' Comparaison of Griffith and Lipfield on TDCB specimen. Produce figure 6 of CMAME paper''' 
hc  = 0.5 #mm
eperlc = 8 #number of element per lc
E = 3500 #MPa
nu = 0.32
KIc =  1.4 #MPa sqrt(m)
Gc  = (1-nu**2)*KIc**2/E*1000#MPa.mm
lc  = eperlc*hc  #~8*h near the crack
Yc  = Gc/4./lc#MPa
eta = 0.1
lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))



def computeGriffith(law, respath, logger):
    respath = respath/'Griffith'
    os.makedirs(respath, exist_ok = False)
    logger.info('Starting computeGriffith')
    logger.info('All results saved in ' + str(respath))    
    meshlevel = 'r1'
   
    # load mesh
    griffithmeshfilename = "../msh/tdcb_2lines_"+meshlevel+".msh"
    mesh = simplexMesh.readGMSH(griffithmeshfilename)
    mech = mech2d.Mechanics2D(mesh, law, None, 0.) 
    # set rigid body conditions on the 2 holes
    rbs = mech2d.rigidBodyConditions(mesh)
    uref =1.
    idl0 = [103] #id of fixed line
    idl1 = [203]  #id of pulled  line
    rb0_id = rbs.addRigidBody(idl0)
    rb1_id = rbs.addRigidBody(idl1)
    rbs.updateRigidBody(rb0_id, {'x':0., 'y': 0.})
    rbs.updateRigidBody(rb1_id, {'x':0., 'y': uref})
    bc = mech2d.boundaryConditions()
    bc.rigidbodies = rbs
   
    # PAir vertices on the cracklines
    idcrackline1 = 100
    idcrackline2 = 200
    crackverticespairing = mesh.link2LinesVertices(idcrackline1, idcrackline2, sortaxis = np.array([1.,0.]))
    nbverticesoncrack = len(crackverticespairing)
    nbedgeoncrack = nbverticesoncrack-1
    crackedgelenghts = np.zeros(nbedgeoncrack+1)
    #compute the lenght of each edge along the crack
    for i in range (0,nbedgeoncrack):
        i0 = crackverticespairing[i][0]
        i1 = crackverticespairing[i+1][0]
        v0 = mesh.xy[i0]
        v1 = mesh.xy[i1]
        crackedgelenghts[i] = np.linalg.norm(v1-v0)

    getrefload = lambda  :   rbs.getRigidBody(rb1_id)['Reactions'][1]
    
    with open(respath / "model", "wb") as filehandler : pickle.dump(mech,filehandler)
    
    
    a  = 0.  # crack lenght
    ua  = 0. #imposed displacement
    Fa  = 0. # computed load (vertical resultant reaction on one of the hole )
    Ea = 0.  # stored strain energie
    ta, tua, tFa, tEa =  [a], [ua], [Fa], [Ea]
    
    u, ignore = mech.zeros()
    
    f = liplog.stepArraysFiles(respath/ 'fields')
    f.save(0, a =np.array([a]), ua = np.array([ua]), Fa =  np.array([Fa]), Ea = np.array([Ea]) ,  u = u)
    
    
        
    for step in range(len(crackverticespairing)) : 
        logger.info('Griffith analysis step %d'%step + ' cracklenght %.2e'%a+' nb attached nodes : %d'%len(crackverticespairing))
        bc.linearconstraints = mech.displacementContraintsOnNodePairs(crackverticespairing) 
        if len(crackverticespairing) == 0 : bc.rigidbodies.updateRigidBody(1,{'teta':0.})
        res = mech.solveDisplacementFixedDLinear(bc=bc, solveroptions = {'linsolve':'umfpack'}) 
        
        u  = res['u']
        Fa = getrefload()
        Ea  = mech.energy(mech.strain(u))       
        f.save(step+1, a =np.array([a]), ua= np.array([uref]), Fa =  np.array([Fa]), Ea = np.array([Ea]), u = u)
         
        
        ta.append(a)  
        tua.append(uref)
        tFa.append(getrefload())
        tEa.append(Ea)
       
        if (len(crackverticespairing)):  crackverticespairing.pop(0)
        if (len(crackverticespairing)==1): crackverticespairing.pop(0)
        a += crackedgelenghts[step]
        
    savegcurvef = liplog.stepArraysFiles(respath/'cracklenght_reaction') 
    savegcurvef.save(0, a = np.array(ta), ua = np.array(tua), Fa = np.array(tFa), Ea = np.array(tEa))  


def computeLip(law, lc, loadingparametervalues, respath, logger):

    respath = respath/'Lip'
    os.makedirs(respath, exist_ok = False)
    logger.info('Starting computeLip')
    logger.info('All results saved in ' + str(respath))
    
    
    solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
    solverdispoptions = {'linsolve':'umfpack'}
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch
    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        }
    alternedsolveroptions= {'abstole':1.e-7, 'reltole':1.e-4, 'deltadtol':1.e-4, 'outputalliter':False, 'verbose':False,  'stoponfailure':False}
    
    
    meshlevel = 'r1'
    meshfilename = "../msh/tdcb_"+meshlevel+".msh"
    mesh = simplexMesh.readGMSH(meshfilename)
    lmesh = dualLipMeshTriangle(mesh)
    lipproj = lipprojector.damageProjector2D(lmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc) 
    
    rbs = mech2d.rigidBodyConditions(mesh)
    uref =1.
    idl0 = [103] #id of fixed line
    idl1 = [203]  #id of pulled  line
    rb0_id = rbs.addRigidBody(idl0)
    rb1_id = rbs.addRigidBody(idl1)
    rbs.updateRigidBody(rb0_id, {'x':0., 'y': 0.})
    rbs.updateRigidBody(rb1_id, {'x':0., 'y': uref})
    bc = mech2d.boundaryConditions()
    bc.rigidbodies = rbs
    
    timer = liplog.timer()
    u,d = mech.zeros()
    a  = 0. # crack lenght
    ua  = 0.   #imposed displacement
    Fa  = 0. # computed load (vertical resultant reaction on one of the hole )
   
    ta, tua, tFa =  [a], [ua], [Fa]
    
    def setloading(val) : 
        rbs.updateRigidBody(rb1_id,{'y': val})
    
    getreaction = lambda  :   rbs.getRigidBody(rb1_id)['Reactions'][1]
    
    
    
    f = liplog.stepArraysFiles(respath/ 'fields')
    
    with open(respath / "model", "wb") as filehandler : pickle.dump(mech,filehandler)
    
    for step, ui in enumerate(loadingparametervalues) :  
        logger.info('#Start Step %06d'%step + ', loading parameter = '+ str(ui))
        timer.new_increment()
        timer.start('total') 
        setloading(ui)  

        dmin = d.copy() 

        res = mech.alternedDispDSolver(dmin = dmin, dguess= d.copy(), un=u,  
                                       bc= bc,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions, timer =timer)
        if not (res['Converged']) : 
            logger.error('lipAnalysis :  alternedDispDSolver did not converge' )
            raise
            
        logger.info('Step %06d'%step + ' Converged in ' + str(res['iter']) + ' iterations.')
        u, d = res['u'], res['d']
        F = getreaction()
       
        a = mech.integrate(d)/(lc)
        ua = ui
        Fa = F
        f.save(step, a =np.array([a]), ua= np.array([ui]), Fa =  np.array([Fa]), u = u, d = d)
        
        tua.append(ua)
        ta.append(a)
        tFa.append(F)
        timer.end('total')
        timer.log(logger)
    
    savegcurvef = liplog.stepArraysFiles(respath/'cracklenght_reaction') 
    savegcurvef.save(0, a = np.array(ta), u = np.array(tua), F = np.array(tFa))  
    

def postGriffith(respath, Gc):
    with liplog.stepArraysFiles(respath/'Griffith'/'cracklenght_reaction').load(0) as res :
        u , F, a = res['ua'], res['Fa'], res['a']       
    E = (F*u)/2.
    ac = np.zeros(a.shape)
    uc = np.zeros(a.shape)
    Fc = np.zeros(a.shape)
    for i in range (2,len(F)-1):
        da = a[i+1] - a[i-1]
        Guimp = (E[i+1] - E[i-1])/da
        Fuimp =  F[i]
        uimp  =  u[i] 
        ac[i] =  a[i]
        uc[i] =  uimp*np.sqrt(-Gc/Guimp)
        Fc[i] =  Fuimp*(uc[i]/uimp)  
    return uc, Fc, a
    
def postLip(respath, lc):
   with  liplog.stepArraysFiles(respath/'Lip'/'cracklenght_reaction').load(0) as res :
       u, F, a = res['u'][:-1], res['F'][:-1], res['a'][:-1]
   for i in range(len(a)):
        Area = a[i] *lc
        a[i] = max((Area - (7./8.)*(1./3)*np.pi*lc*lc)/lc, 0.)
    #hack
   return u[1:], F[1:], a[1:]
   return u, F, a
    
def plotComparaison(respath, Gc, lc, fig = None, ax = None, marksteps = [120]):
    if (ax is None or fig is None) :
        fig, ax1 = plt.subplots()
    else :
        ax1 = ax
    
    Gu, GF, Ga  = postGriffith(respath, Gc)    
    Lu, LF, La  = postLip(respath, lc)       
        
    colora = 'tab:red'
    colorf =  'tab:blue'
    ax1.set_xlabel(r'Imposed displacement $y\; [mm]$')
    ax1.set_ylabel(r'Load $[N/mm]$', color=colorf)  # we already handled the x-label with ax1
    #ax1.set_xlim(0., gu.max()) 
    ax1.plot(Gu[:-1], GF[:-1], label = 'Griffith', color=colorf)
    for s in marksteps :
        ax1.plot(Gu[s], GF[s], 'o', color=colorf)
    
    ax1.plot(Lu, LF, label ='Lip-field', linestyle ='dashed', color=colorf)
    for s in marksteps :
        ax1.plot(Lu[s], LF[s], 'o', linestyle ='dashed', color=colorf)
    
    ax1.tick_params(axis='y', labelcolor=colorf)
    #ax1.legend(title = 'Load', loc='center')
    ax1.legend( loc='center right')
        
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.plot(Gu[:-1], Ga[:-1], label = 'Griffith', color=colora)
    for s in marksteps :
        ax2.plot(Gu[s], Ga[s],'o', color=colora)
    
    ax2.plot(Lu, La, label ='Lip-field', linestyle ='dashed', color=colora)
    for s in marksteps :
        ax2.plot(Lu[s], La[s], 'o', linestyle ='dashed', color=colora)
    
    ax2.tick_params(axis='y', labelcolor=colora)
    ax2.set_ylabel('Crack lenght (mm)', color=colora)
    #ax2.legend(title = 'Crack lenght', loc='upper center')
    #ax1.set_title('Global comparaison between Griffith and Lip-Field')
    ax1.set_xlim(0,2.)
   # fig.tight_layout()  # otherwise the right y-label is slightly clipped

    
    
   
def plotSyyLip(respath, step,  fig =None, ax = None, minSyy = None, maxSyy = None, uscale = 10.):
    respath = respath/'Lip'
    with open(respath / "model", "rb") as filehandler :
         mech = pickle.load(filehandler)
    #hack
    step = step+1
    rstep= liplog.stepArraysFiles(respath/'fields').load(step)
    u, d = rstep['u'], rstep['d']
    Syy = mech.stress(u, d)[:,1]
    
    c, fig, ax = mech.mesh.plotScalarField(Syy, u*uscale, Tmin = None, Tmax = None, fig= fig, ax = ax, showmesh=False)
    minSyy, maxSyy = np.min(Syy), np.max(Syy)
    return c, fig, ax, minSyy, maxSyy

def plotSyyGriffith(respath, step, Gc, fig =None, ax = None, minSyy = None, maxSyy = None, uscale = 10.):
    Gu, GF, Ga  = postGriffith(respath, Gc)  
    respath = respath/'Griffith'
    
    with open(respath / "model", "rb") as filehandler :
         mech = pickle.load(filehandler)
         
    ignore, d = mech.zeros()
    u = liplog.stepArraysFiles(respath/'fields').load(step)['u']    
    uref = liplog.stepArraysFiles(respath/'fields').load(step)['ua'][0]
    
    fac = Gu[step]/uref
    u = u*fac
    Syy =mech.stress(u, d)[:,1]

    c, fig, ax = mech.mesh.plotScalarField(Syy, u*uscale, Tmin = minSyy, Tmax = maxSyy, fig= fig, ax = ax, showmesh=False)
    minSyy, maxSyy = np.min(Syy), np.max(Syy)
    return c, fig, ax, minSyy, maxSyy
    
def plotD(respath, step, fig = None, ax = None):
    #hack
    step = step+1
    respath = respath/'Lip'
    with open(respath / "model", "rb") as filehandler :
         mech = pickle.load(filehandler)
    d = liplog.stepArraysFiles(respath/ "fields").load(step)['d']
    c, fig, ax  = mech.mesh.plotScalarField(d,Tmin = 0., Tmax =1.,  fig = fig, ax = ax, showmesh=False)
    return c, fig, ax



def plot(respath, figpath):
    import os
    os.makedirs(figpath, exist_ok = True)
    fig, axes = plt.subplots(2,3,  figsize = (12,12/3*2) )
    axComp = axes[0,0]
    axGriffith1 = axes[0,1]
    axLip1     = axes[1,1]
    axGriffith2 = axes[0,2]
    axLip2     = axes[1,2]
    axD        = axes[1,0]
    msteps = [20,120]
    plotComparaison(respath, Gc, lc, fig=fig, ax = axComp, marksteps = msteps )
    
    colorbarS1, ignore, ignore, smin, smax = plotSyyLip(respath, msteps[0], fig=fig, ax = axLip1)
    ignore, ignore, ignore, ignore, ignore = plotSyyGriffith(respath, msteps[0], Gc, fig=fig, ax = axGriffith1, minSyy = smin, maxSyy = smax, uscale = 10.)
    
    colorbarS2, ignore, ignore, smin, smax = plotSyyLip(respath, msteps[1], fig=fig, ax = axLip2)
    ignore, ignore, ignore, ignore, ignore = plotSyyGriffith(respath, msteps[1], Gc, fig=fig, ax = axGriffith2, minSyy = smin, maxSyy = smax, uscale =10.)
    
    colorbarD, ignore, ignore = plotD (respath, msteps[1], fig =fig , ax = axD  )

     
    x_min, x_max = +np.inf, -np.inf
    y_min, y_max = +np.inf, -np.inf
    for ax in [axGriffith1, axLip1,axGriffith2, axLip2,  axD] :
        xlim, ylim = ax.get_xlim(), ax.get_ylim()
        x_min, x_max = min(xlim[0], x_min ), max(xlim[1], x_max )
        y_min, y_max = min(ylim[0], y_min ), max(ylim[1], y_max )
    
    for ax in [axGriffith1, axLip1,axGriffith2, axLip2,  axD] :
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.axis('equal')
        ax.set_axis_off()
        ax.set_xlim((x_min, x_max))
        ax.set_ylim((y_min, y_max))
        
    
    divider = make_axes_locatable(axGriffith1)
    cax1 = divider.append_axes("bottom", size="5%", pad=0.05)
    cax1.set_axis_off()
    
    divider = make_axes_locatable(axLip1)
    cax1 = divider.append_axes("bottom", size="5%", pad=0.05)
    #axins = inset_locator.inset_axes(axGriffith, "4%", "100%")
    c1 = fig.colorbar(colorbarS1, cax=cax1, orientation='horizontal')
    c1.set_label(r'$\sigma_{yy} \; [MPa]$' )
    
    divider = make_axes_locatable(axGriffith2)
    cax1 = divider.append_axes("bottom", size="5%", pad=0.05)
    cax1.set_axis_off()
    divider = make_axes_locatable(axLip2)
    cax1 = divider.append_axes("bottom", size="5%", pad=0.05)
    
    #axins = inset_locator.inset_axes(axGriffith, "4%", "100%")
    c1 = fig.colorbar(colorbarS2, cax=cax1, orientation='horizontal')
    c1.set_label(r'$\sigma_{yy}\; [MPa]$' )
    
    
    divider = make_axes_locatable(axD)
    cax1 = divider.append_axes("bottom", size="5%", pad=0.05)
    #axins = inset_locator.inset_axes(axGriffith, "4%", "100%")
    c1 = fig.colorbar(colorbarD, cax=cax1, orientation='horizontal')
    c1.set_label(r'$d$' )
    c1.set_ticks([0.,0.5,1.])
    
    fig.savefig(figpath/"Fig_6_TDCB.png", format = 'png', dpi =400, bbox_inches='tight')
    fig.savefig(figpath/"Fig_6_TDCB.pdf", format = 'pdf', bbox_inches='tight')



from mpl_toolkits.axes_grid1 import make_axes_locatable
if __name__ == '__main__':
    import sys
    respath = pathlib.Path.home()/pathlib.Path('tmp/resultsTDCB')
    figpath = pathlib.Path('figures')
    if len(sys.argv )> 1:  respath = pathlib.Path(sys.argv[1]) 
    
    compute  = True
    
    law = mlaws.SofteningElasticitySymmetric(lamb, mu, Yc = Yc, h = mlaws.HQuadratic(),  g = mlaws.GO4Eta(eta))
    
    if compute :
        os.makedirs(respath, exist_ok = False)
        shutil.copy(__file__, respath / "Run_script.py")
        logger = liplog.setLogger(respath / "TDCB.log")    
        computeGriffith(law, respath, logger)
        guc, gFc, gac = postGriffith(respath, Gc)
        #compute with lipfield, for imposed displacement on the circle identical as the displacement obtained with griffith analysis
        computeLip(law, lc, guc, respath, logger)
    
    plot(respath, figpath)
   
    #fig, axes = plt.subplots(2,2,  figsize = set_size(1000) )
    #fig, axes = plt.subplots(2,3,  figsize = (1.2*21./2.54, 21./3*2/2.54) )

    #post(respath)
  