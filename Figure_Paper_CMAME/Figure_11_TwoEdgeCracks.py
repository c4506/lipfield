#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#Solve lipfield for a 2d square plate with hole in plane strain.
#mesh created from hole.geo. a square of lenght 2L, with a circular hole at the center of radius R.

#note cohesive model : lm = 2.*Yc*lc/Gc



import os
import matplotlib.pylab as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from   lip2d.mesh import simplexMesh, dualLipMeshTriangle
import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector
import lip2d.liplog as liplog
#import datetime
import shutil
import pathlib
import pickle



def compute(respath):
   
    os.makedirs(respath, exist_ok = False)
    shutil.copy(__file__, respath/'Run_script.py')
    logfilepath = respath/'two_edge_crack.log'
    logger = liplog.setLogger(logfilepath)   
    logger.info('Starting Program two_edge_cracks. All results saved in '+str(respath.absolute()))
    logger.info('Log file : '+str(logfilepath.absolute()))    
    
    meshid ='4'
    basemeshname = 'two_edge_cracks_r'+meshid
    basemeshname = 'two_edge_cracks_refine_loc'
    
    E = 1.
    nu = 0.3
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    eta = 0.1
    lc = 0.01 # lenght scaleof the lip constrain
    
    onlineplot = False
    
    savefilename = respath / "fields"
    fieldsave  = liplog.stepArraysFiles(savefilename)    
    plt.close('all')
    
    # Define the material law 
    law = mlaws.SofteningElasticitySymmetric(lamb, mu,  h = mlaws.HQuadratic(), g = mlaws.GO4Eta(eta))
    
    # Solver for equilibrium
    solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
    solverdispoptions = {'linsolve':'cholmod'}
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
    
    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}}
        
    alternedsolveroptions= {'abstole':1.e-8, 
                            'reltole':1.e-5,  
                            'deltadtol':1.e-5, 
                            'outputalliter':True, 
                            'verbose':False, 
                            'stoponfailure':False}
    

    
    meshfilepath = pathlib.Path('../msh/'+basemeshname+'.msh')
    mesh = simplexMesh.readGMSH(meshfilepath)
    nv = mesh.nvertices
    nf =  mesh.ntriangles
    logger.info('Mesh Loaded from file ' + str(meshfilepath.absolute()))
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    
    lipproj = lipprojector.damageProjector2D(lipmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
         
    idv0 = 1 # id of fixed point
    fixedlineid = 100#id of fixed line
    pulledlineid = 101#id of pulled line
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([fixedlineid],{'y':0.})
    did = bc.dirichlets.add([pulledlineid],{'y':0.})
    bc.dirichlets.addPoints([idv0],{'x':0, 'y':0.})
    
    timer = liplog.timer()
    u, d= mech.zeros()
    
    uimp=[]
    Fy  =[]
    etab= []
    showmesh = nv < 1000
    
    with open(respath / "model", "wb") as filehandler :
        pickle.dump(mech,filehandler)

    incs = np.linspace(0.,6.,301)
    
    for step, ui in enumerate(incs) : 
        logger.info('#Start uimp = '+ str(ui))
        timer.new_increment()
        timer.start('total') 
        bc.dirichlets.update(did, {'y':ui})
        dmin = d.copy() 
        res = mech.alternedDispDSolver(dmin = dmin, dguess= d.copy(), un=u,
                                       bc=bc,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions,
                                       timer=timer)
        dtmp1 = res['d']
        dmind1 = np.linalg.norm(dtmp1-dmin)
        logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dmin-d1|= ' + '%2.e'%dmind1)
        u = res['u']
        d = res['d']
        R = res['R']
        if onlineplot :     mech.plots(u, d, ui, savefilename, fields =np.array([['d_1','stressyy' ],['Y', 'd']]),  showmesh = showmesh) 
        if not (res['Converged']) : raise
        
        strain = mech.strain(u)
        e = mech.energy(strain, d)
        Ry = sum(bc.dirichlets.getReactions(did, R))
        uimp.append(ui)
        etab.append(e)
        Fy.append(Ry)
        fieldsave.save(step,  u = u, d = d, R = R)   
        
        timer.end('total')
        timer.log(logger)
        
    f =  open(respath/'RU.npy', 'wb')
    np.save(f,np.array(uimp))
    np.save(f,np.array(Fy))
    f.close()
    
    fig, ax = plt.subplots(1,1)
    fig.suptitle(r'$F(u)$')
    plt.plot(uimp, np.array(Fy), '-o')
    ax.set_xlabel(r'Imposed Displacement')
    ax.set_ylabel(r'Reaction Force')
    fig.savefig(respath/'Load_Displacement.pdf', format = 'pdf')    
    
def Figure_11(respath):
    
    with open(respath / "model", "rb") as filehandler :
        mech = pickle.load(filehandler)
        
    loadfilename = respath / "fields"
    fieldload = liplog.stepArraysFiles(loadfilename)
    with open(respath/'RU.npy', 'rb') as f:
       uimp =  np.load(f)
       Ry =    np.load(f)
       
    import FiguresCommon
    fig, axcurve, axesd, caxbar = FiguresCommon.createFigAxesCurve5Map()
    
    
    axcurve.set_xlabel(r'Imposed Displacement (mm)')
    axcurve.set_ylabel(r'Reaction Force (N/mm)')
    axcurve.plot(uimp, Ry,color='blue')
   # axcurve.plot(utab, Ftab,'-')
    
    
    def myplotstep(step, fig, ax):
        #logger.info('Plotting Step %d'%step)
        rstep =  fieldload.load(step)
        #ux = incs[step]
        u  = rstep['u']
        d  = rstep['d']
        #stress = mech.stress(u, d)
        cd, figd, axd  = mech.mesh.plotScalarField(d, u*0.,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = ax)
        
        axd.set_axis_off()
        axd.axis('equal')
        #axd.set_title(r"$D$") 
        axcurve.plot(uimp[step], Ry[step],'o',color='blue')
        return cd
       
    steps = [16,17,30,80,300]
    for step, ax in zip(steps, axesd):
        cbard = myplotstep(step, fig, ax)
        

    cbard = fig.colorbar(cbard, cax= caxbar, orientation='horizontal')
    cbard.set_label(r'$d$')
    cbard.set_ticks([0.,0.5,1.])
   
    fig.tight_layout
    fig.show()
    return fig


def post_step(respath, step, name, uscale = 0.01):

    with open(respath / "model", "rb") as filehandler :
        mech = pickle.load(filehandler)
        
    fieldload  = liplog.stepArraysFiles(respath / "fields")
    rstep =  fieldload.load(step)
    u = rstep['u']
    d = rstep['d']
    stress = mech.stress(u, d)
    #mech.plots(u, d, ui, name, fields =np.array([['d_1','stressyy' ],['Y', 'd']]),  showmesh = showmesh) 
    mesh = mech.mesh
    #lipmesh = mech.lipprojector.mesh
    cl, figl, axl = mesh.plotScalarField(stress[:,1], u*uscale, showmesh=False)
    cl, figl, axl = mesh.plotScalarField(d, u*uscale, showmesh=False)
    
def plot(respath, figpath):
    import os
    os.makedirs(figpath, exist_ok = True)
    fig11 = Figure_11(respath)
    fig11.savefig(figpath/'Fig_11_TwoEdgeCracks.pdf', format = 'pdf', bbox_inches='tight')
    fig11.savefig(figpath/'Fig_11_TwoEdgeCracks.png', format = 'png', dpi =400, bbox_inches='tight')
    
#convert -quality 50 *.png outputfile.mpeg
       
if __name__ == '__main__':
    import sys
    respath = pathlib.Path.home()/pathlib.Path('tmp/resultsTwoEdgesCrack')
    figpath = pathlib.Path('figures')
    run =True
    if len(sys.argv )> 1:
       # now = datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")
        respath = pathlib.Path(sys.argv[1]) 
       
    if run :
        respath = compute()
    
    plot(respath, figpath)
    
    
        
