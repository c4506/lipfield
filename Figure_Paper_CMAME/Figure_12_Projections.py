#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 16:34:01 2021
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Tue Aug 24 15:38:21 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import lip2d.lipdamage as lip2D
from lip2d.mesh import simplexMesh
import pylab as plt
import pathlib
import os

def plot(figpath):
    ''' Produce figure 12 of CMAME Paper, example of lipprojection, illustrating 2 different discrete lip space'''
    # plt.matplotlib.rcParams.update({
    #     "pgf.texsystem": "pdflatex",
    #     'font.family': 'serif',
    #     'text.usetex': True,
    #     'pgf.rcfonts': False,
    # })
    errors = []
    meshfilenames = ['msh/Square_structured_r0.msh', 'msh/Square_structured_r1.msh', 'msh/Square_structured_r2.msh',
                     'msh/Square_structured_r3.msh'] #, '../msh/Square_structured_r4.msh']
    meshfilename =  meshfilenames[3]
    os.makedirs(figpath, exist_ok = True)
    
    
    def compute(meshfilename, plot, doedge, fig, ax_map_exact, ax_map_edge, ax_map_tri, ax_cut_edge, ax_cut_tri, ax_color_map):
        m = simplexMesh.readGMSH(meshfilename)
        nv = m.nvertices
        lc = 1.
        lctarget = lc/4.
        
        def cone(x, topval, lc):
            r= np.sqrt(x[:,0]**2 + x[:,1]**2)
            d = topval-r/lc
            return np.where(d>0., d, 0.)
            
        dtarget = cone(m.xy,1.,lctarget)
        dtopopt = (1./16)**(1/3)
        lip = lip2D.damageProjector2D(m)
        dmin = np.zeros(nv)
        dlipt = lip.lipProjClosestToTarget(dmin, dtarget, lc, init = None, lipmeasure = 'triangle', kktsolveroptions =  {'mode':'direct', 'linsolve':'umfpack'})
       
        if (plot) :
            c, f, a = m.plotScalarField(dlipt, showmesh=False,  fig =fig , ax = ax_map_tri)
            a.axis('equal')
            a.set_title(r'Projection on $\mathcal{L}^h$')
            a.set_axis_off()
            
        dlipe = lip.lipProjClosestToTarget(dmin, dtarget, lc, init = None, lipmeasure = 'edge', kktsolveroptions =  {'mode':'direct', 'linsolve':'umfpack'})
    
        if (plot and doedge) :
            c ,f, a  = m.plotScalarField(dlipe, showmesh=False, fig =fig , ax = ax_map_edge)
            a.axis('equal')
            a.set_title(r'Projection on $\mathcal{L}^{h+}$')
            a.set_axis_off()
        
        if plot :
            for theta, thetaname in  zip([0., np.pi/4], [r'$\theta = 0$', r'$\theta = \frac{\pi}{4}$']) :
        
                P0 = np.array([np.cos(theta),np.sin(theta)])
                P1 = -P0
                extract_dlip = m.extractScalarVertexFieldOnLine(dlipe, P0, P1, 201 )
                edlipe = extract_dlip['v']
                extract_dlip = m.extractScalarVertexFieldOnLine(dlipt, P0, P1, 201 )
                edlipt = extract_dlip['v']
                extract_dlip = m.extractScalarVertexFieldOnLine(dtarget, P0, P1, 201 )
                edtarget = extract_dlip['v']
                xy = np.column_stack([extract_dlip['x'], extract_dlip['y']])
                r = np.linspace(-1.,1., 201)
        
                if plot :
                    if theta == 0. :
                        if doedge : 
                            ax_cut_edge.plot(r, edtarget, label = r'$d$')
                            ax_cut_edge.plot(r, cone(xy, dtopopt, lc), label = r'$\pi_{\mathcal{L}}(d)$ ' )
                        ax_cut_tri.plot(r, edtarget, label = r'$d$')
                        ax_cut_tri.plot(r, cone(xy, dtopopt, lc), label = r'$\pi_{\mathcal{L}}(d)$')
                        
                    if doedge :ax_cut_edge.plot(r, edlipe, label= r'$\pi_{\mathcal{L}^{h+}}(d)$, '+thetaname)
                    ax_cut_tri.plot(r, edlipt, label = r'$\pi_{\mathcal{L}^{h}}(d)$, ' +thetaname)
                   
                #ax.plot(xy[:,0], cone(xy, 1., lctarget), label = 'target')
                
            ax_cut_tri.legend()
            ax_cut_edge.legend()
              
            ax_cut_edge.set_xlabel(r'$x$')
            ax_cut_edge.tick_params(top=False, bottom=True, left=False, right=False, labelleft=False, labelbottom=True)
            ax_cut_edge.set_xticks([-1., 0., 1.])
            ax_cut_tri.set_xlabel(r'$x$')
            
            ax_cut_tri.yaxis.set_label_position("right")
            ax_cut_tri.yaxis.tick_right()
            ax_cut_tri.set_yticks([0.,0.5,1.])
            ax_cut_tri.set_xticks([-1., 0., 1.])
            
            ax_cut_tri.set_ylabel(r'$d$')
    
        dexactmesh = cone(m.xy, dtopopt, lc)
        if plot :
            c, f, a = m.plotScalarField(dexactmesh, Tmin =0., Tmax=0.4, showmesh=False, fig=fig, ax =ax_map_exact)
            a.axis('equal')
            
            
            a.set_title(r'Projection on $\mathcal{L}$' )
            a.set_axis_off()
            #ax_color_map.set_axis_off()
            cb = f.colorbar(c, cax=ax_color_map, orientation = 'vertical',  ticklocation = 'left')
            cb.set_ticks([0.,0.2,0.4])
            cb.set_label(r'$d$')
            
        #intexact = m.integrateVertexField( dexactmesh)
        #inttarget = m.integrateVertexField( dtarget)
        #print('intexact', intexact)
        #print('inttarget', inttarget)
    
        exactdin =  np.sqrt( m.integrateVertexField( (dexactmesh)**2 ))
        edgeerror = np.sqrt( m.integrateVertexField( (dexactmesh-dlipe)**2 ))
        triangleerror = np.sqrt( m.integrateVertexField( (dexactmesh-dlipt)**2 ))
        #print(edgeerror/exactdin*100, triangleerror/exactdin*100, exactdin)
        #print(m.integrateVertexField(np.ones(m.nvertices)))
    
        if doedge :   return {'eerror':edgeerror/exactdin, 'terror':triangleerror/exactdin}
        else : return{'terror':triangleerror/exactdin}
        
    errors =[]
    
    doedge = True

    fig = plt.figure(figsize=(9.7,6.))
    a = 2
    b = a*15
    ax_color_map =        fig.add_subplot(2,3*b+a,  (1,a))
    ax_map_exact = fig.add_subplot(2,3*b+a, (a+1,b+a))
    ax_map_edge  = fig.add_subplot(2,3*b+a, (b+a+1,2*b+a))
    ax_map_tri   = fig.add_subplot(2,3*b+a, (2*b+a+1,3*b+a))
    
    ax_conv =            fig.add_subplot(2,3*b+a,  (3*b+2*a+1, 4*b+2*a-a//2))
    ax_cut_edge =    fig.add_subplot(2,3*b+a, (4*b+2*a+1+a//2,5*b+2*a-a//2))
    ax_cut_tri =    fig.add_subplot(2,3*b+a,  (5*b+2*a+1+a//2,6*b+2*a-a//2))
    
    for meshfilename in  meshfilenames:
        errors.append(compute(meshfilename, meshfilename == 'msh/Square_structured_r3.msh',  doedge, fig, ax_map_exact, ax_map_edge, ax_map_tri,    ax_cut_edge, ax_cut_tri, ax_color_map ))
        #print(dtopopt*np.pi*(dtopopt*lc)**2/3., m.integrateVertexField(dexactmesh))
    
    #pr.print_stats(sort = 'cumulative')
    
    errst = [ e['terror'][0] for e in errors ]
    if doedge : errse = [ e['eerror'][0] for e in errors ]
    #print(errst)
    #print(errse)
    h_L = [1./4./(2**i) for i in range(len( meshfilenames) ) ]
    #h_L = [1./4, 1./8., 1./16., 1./32, 1./64]
    
    ax_conv.loglog(h_L, errst, '-o', label =r'$\pi_{\mathcal{L}^h}(d)$' )
    ax_conv.loglog(h_L, errse,'-o', label =r'$\pi_{\mathcal{L}^{h+}}(d)$' )
    ax_conv.set_xlabel(r'$h/L$')
    ax_conv.set_ylabel(r'$L^2$ relative error ')
    ax_conv.set_xlim([1.e-2,1.])
    ax_conv.set_ylim([1.e-2,1.])
    ax_conv.legend()
    fig.savefig( figpath/'Fig_12_Annexe.pdf', format='pdf', bbox_inches='tight') 
    fig.savefig( figpath/'Fig_12_Annexe.png', format='png', dpi =400, bbox_inches='tight')
    
    
if __name__ == '__main__':
    figpath = pathlib.Path('figures')
    plot(figpath)


