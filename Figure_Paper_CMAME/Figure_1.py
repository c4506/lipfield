#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Tue Aug 24 15:38:21 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pathlib

def plot(figpath):
    ''' plot the 0d damage law, Figure 1 of CMAME paper'''
    import numpy as np
    import scipy.integrate
    import pylab as plt
    import lip2d.material_laws_planestrain as laws
    import os
    os.makedirs(figpath, exist_ok = True)
    
    plt.matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })

    E = 3500 #Mpa
    nu = 0.32
    KIc =  1.4 #Mpa sqrt(m)
    Gc  = (1-nu**2)*KIc**2/E*1000#Mpa.mm
    lc = 1.
    #lc  = eperlc*hc  #~8*h near the crack
    Yc  = Gc/4./lc#Mpa
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    eta = 1./3.
    h =  laws.HQuadratic()
    fig, axs = plt.plt.subplots(1,1,  figsize = (4,3) )
    axd = axs.twinx() 
    for eta in[0.,0.1,0.2,0.3] :
        d = np.array([0.])
        geta =  laws.GO4Eta(eta)
        law = laws.SofteningElasticitySymmetric(  lamb, mu, Yc = Yc, h =h, g = geta) 
        
        dh =[]
        s11=[]
        e11=[]
        for eps11 in np.linspace(0,10., 501) :
            eps11 *= np.sqrt(Yc / (0.5*lamb +  mu))
            eps22 = 0.
            eps12 = 0.
            strain = np.array([[eps11, eps22, 2*eps12]])
            d = law.solveSoftening(strain, np.array(d))
            stress = law.solveStressFixedSoftening(strain, d, deriv = False)[0]
            dh.append(d[0])
            e11.append(strain[0,0])
            s11.append(stress[0,0])
            
            
        ll = axs.plot(e11/np.sqrt(Yc / (0.5*lamb +  mu)), s11 / (2.*np.sqrt(Yc*(0.5*lamb + mu))), label = r'$\eta = $ %.2f'%eta)
        axd.plot(e11/np.sqrt(Yc / (0.5*lamb +  mu)), dh, color=ll[-1].get_color())
        
    
    axs.set_xlabel(r"$\epsilon_{11}/\sqrt{2Y_c/(\lambda+2\mu)}$")
    axs.set_ylabel(r"$\sigma_{11}/(2 \sqrt{Yc(\lambda + 2\mu)/2})$")
    axd.set_ylabel(r"$d$")
    axs.legend(loc='center right')
    fig.tight_layout()
    basename = 'Fig_1.'
    for forma in ['pdf', 'pgf', 'svg', 'png'] :
        fig.savefig(figpath/pathlib.Path(basename+forma), format = forma, bbox_inches='tight') 
        

if __name__ == '__main__':
    figpath = pathlib.Path('figures')
    plot(figpath)