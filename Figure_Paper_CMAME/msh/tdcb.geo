//units :  millimeter
// hc = 1.; // _r0 edge size along the crack line
//hc= 0.5;    // _r1 edge size along the crack line
//hc = 0.25; // _r2 edge size along the crack line
hc = 0.125; // _r3 edge size along the crack line

hf = 4.*hc; // edge size along the other lines

L1= 100.;
L2 = 12.;
L3 = 20.;
L4 = 24.;
H1 = 70.;
H2 = 90.;
H3 = 24.;
H4 = 5.;
R  = 4.;

Point(1)  = {L4,       0.,   0, hc};
Point(2)  = {L3,    H4/2.,   0, hf};
Point(3)  = {0.,    H4/2.,   0, hf};
Point(4)  = {0.,    H1/2.,   0, hf};
Point(5)  = {L1,    H2/2.,   0, hf};
Point(6)  = {L1,   -H2/2.,   0, hf};
Point(7)  = {0.,   -H1/2.,   0, hf};
Point(8)  = {0.,   -H4/2.,   0, hf};
Point(9)  = {L3,   -H4/2.,   0, hf};
Point(10) = {L1,       0.,   0, hc};

Point(11) = {L2,   -H3/2.,   0, hf};
Point(12) = {L2,    H3/2.,   0, hf};

Point(13) = {L2+R, -H3/2.,   0, hf};
Point(14) = {L2,   -H3/2.+R, 0, hf};
Point(15) = {L2-R, -H3/2.,   0, hf};
Point(16) = {L2,   -H3/2.-R, 0, hf};

Point(17) = {L2+R,  H3/2.,   0, hf};
Point(18) = {L2,    H3/2.+R, 0, hf};
Point(19) = {L2-R,  H3/2.,   0, hf};
Point(20) = {L2,    H3/2.-R, 0, hf};

//+
Line(1) = {8, 7}; 
//+
Line(2) = {7, 6};
//+
Line(3) = {6, 10};
//+
Line(4) = {10, 5};
//+
Line(5) = {5, 4};
//+
Line(6) = {4, 3};
//+
Line(7) = {3, 2};
//+
Line(8) = {2, 1};
//+
Line(9) = {1, 9};
//+
Line(10) = {9, 8};
//+
Line(11) = {1, 10};
//+
Circle(12) = {20, 12, 17};
//+
Circle(13) = {17, 12, 18};
//+
Circle(14) = {18, 12, 19};
//+
Circle(15) = {19, 12, 20};
//+
Circle(16) = {16, 11, 13};
//+
Circle(17) = {13, 11, 14};
//+
Circle(18) = {14, 11, 15};
//+
Circle(19) = {15, 11, 16};
//+
Curve Loop(1) = {10, 1, 2, 3, -11, 9}; //bot
//+
Curve Loop(2) = {17, 18, 19, 16};
//+
Plane Surface(1) = {1, 2};
//+
Curve Loop(3) = {7, 8, 11, 4, 5, 6}; // top
//+
Curve Loop(4) = {12, 13, 14, 15};
//+
Plane Surface(2) = {3, 4};



Physical Surface("Sbot", 1000) = {1};
Physical Surface("Stop", 2000) = {2};

//+
Physical Curve("circletop", 103) = {16, 17, 18, 19};
//+
Physical Curve("circlebot",203) = {12, 13, 14, 15};


//+
Physical Curve("externbound",2001) = {2, 3, 4, 5, 6, 7, 8, 9, 10, 1};
