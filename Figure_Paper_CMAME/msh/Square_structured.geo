//+
//d  = 0.125; //ref3
//d  = 0.0625; //ref4
d = 0.03125; //ref5
L  = 1.;
N = 9;  //h = L/4
N = 17; //h = L/8
N = 33; //h = L/16
N = 65; //h = L/32
N = 129; //h = L/64

Point(1) = {-L, -L, 0., d};
Point(2) = {L, -L, 0., d};
Point(3) = { L, L, 0., d};
Point(4) = {-L, L, 0., d};



//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Curve(10) = {1};
//+
Physical Curve(11) = {2};
//+
Physical Curve(12) = {3};
//+
Physical Curve(13) = {4};
//+
Physical Surface(100) = {1};

Transfinite Line{1,2,3,4} = N Using Progression 1;
Transfinite Surface{1};
