#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

#Solve lipfield for a 2d square plate with hole in plane strain.
#mesh created from hole.geo. a square of lenght 2L, with a circular hole at the center of radius R.
# Running time : ~60s (with the default mesh (meshid ='2'))


import os
os.environ["OMP_NUM_THREADS"] = "1"
os.environ["OPENBLAS_NUM_THREADS"] = "1"

import matplotlib.pyplot as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from   lip2d.mesh import simplexMesh, dualLipMeshTriangle
import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipreg
import lip2d.liplog    as liplog
import datetime
import shutil
import pathlib
import pickle



def compute(respath = pathlib.Path('resHoleDamage')
 / pathlib.Path(datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")), pr = None):
    meshid ='2'
    basemeshname = 'hole_ref'+meshid
    lc = 0.2 # lenght scaleof the lip constrain
    E = 1.
    nu = 0.3
    eta = 0.1
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    onlineplot = False
    plotpatches = False
    plt.close('all')
    os.makedirs(respath, exist_ok = False)
    shutil.copy(__file__, respath / "Run_script.py")
    logger = liplog.setLogger(respath / "Hole_Damage.log")       
    logger.info('Starting Program Hole_damage')
    logger.info('All results saved in ' + str(respath))
    
    meshfilename = '../msh/'+basemeshname+'.msh'
    savefilename = respath / "fields"
   
    fieldsave  = liplog.stepArraysFiles(savefilename)
    
    law = mlaws.SofteningElasticitySymmetric(lamb, mu,
                                            h = mlaws.HQuadratic(), 
                                            g = mlaws.GO4Eta(eta))
    solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
    solverdispoptions = {'linsolve':'cholmod'}
    
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch
    
    solverdlipoptions ={'kernel': lipreg.minimizeCPcvxopt,
                        'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        }
    
    alternedsolveroptions= {'abstole':1.e-8, 
                            'reltole':1.e-5,  
                            'deltadtol':1.e-5, 
                            'outputalliter':True, 
                            'verbose':False, 
                            'stoponfailure':False,
                            'maxiter': 10000
                            }
    
    mesh = simplexMesh.readGMSH(meshfilename)
    nv = mesh.nvertices
    nf =  mesh.ntriangles
    logger.info('Mesh Loaded from file ' + meshfilename)
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    
    lipproj = lipreg.damageProjector2D(lipmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
         
    idfix  = 10#id of fixed line
    idpull = 12#id of pulled line
    
    
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([idfix],{'x':0., 'y':0.})
    bc.dirichlets.add([idpull],{'y':0.})
    did = bc.dirichlets.add([idpull],{'x':0.})
    
    timer = liplog.timer()   

    with open(respath / "model", "wb") as filehandler :
        pickle.dump(mech,filehandler)
              
    
    u= np.zeros((nv, 2))
    d = np.zeros((mesh.ntriangles))
    
    uimp=[]
    Fx  =[]
    etab= []
    showmesh = nv < 1000
    fig0, ax0 = plt.subplots(1,1)
    fig0.suptitle(r'$F(u)$')
    
    ax0.set_xlabel(r'Imposed Displacement')
    ax0.set_ylabel(r'Reaction Force')
    
    
    # load increment (displacement of the right boundary)
    incs = [0.,1., 1.25, 1.5, 1.6, 1.7,1.8,1.9,2.,2.1, 2.3,2.5,2.7,2.75,2.8,3., 3.5, 4., 4.5, 5.] 
    for step, ui in enumerate(incs) : 
        timer.new_increment()
        timer.start('total')
        logger.info('#Start uimp = '+ str(ui))
           
        bc.dirichlets.update(did,{'x':ui})
            
        dmin = d.copy() 
        if (pr is not None) : pr.enable()
        res = mech.alternedDispDSolver(dmin = dmin, dguess= d.copy(), un=u, 
                                       bc = bc,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions,
                                       timer =timer)
        if (pr is not None) : pr.disable()
        dtmp1 = res['d']
        dmind1 = np.linalg.norm(dtmp1-dmin)
        logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dmin-d1|= ' + '%2.e'%dmind1)
        u = res['u']
        d = res['d']
        timer.start('post')
        if onlineplot :     mech.plots(u, d, ui, savefilename, showmesh = showmesh) 
        if plotpatches :    mech.plotPatches(res, savefilename, ui)
            
        timer.end('post')
        if not (res['Converged']) : raise
        
        R = res['R']
        strain = mech.strain(u)
        e = mech.energy(strain, d)
    
    
        Rx = sum(bc.dirichlets.getReactions(did, R))
        
        uimp.append(ui)
        etab.append(e)
        Fx.append(Rx)
        fieldsave.save(step,  u = u, d = d)
            
        ax0.plot(np.array(uimp), np.array(Fx), '-o')
        timer.end('total')
        timer.log(logger)
    
   
    
        
    uimp = np.array(uimp)
    with open( respath / 'loadDisplacement.npy','wb') as f :
        np.save(f,uimp)
        np.save(f,Fx)
        f.close()
    
    if onlineplot:
        fig, ax = plt.subplots(1,1)
        fig.suptitle(r'$F(u)$')
        plt.plot(uimp, np.array(Fx), '-o')
        ax.set_xlabel(r'Imposed Displacement')
        ax.set_ylabel(r'Reaction Force')
        
        fig, ax = plt.subplots(1,1)
        fig.suptitle(r'$e(u)$')
        plt.plot(uimp, np.array(etab), '-o')
        ax.set_xlabel(r'Imposed Displacement')
        ax.set_ylabel(r'energy')
   
    return respath
    
def post(respath):
    logger = liplog.setLogger(respath / "Hole_damage_Post.log") 
    logger.info('Starting Post Hole_damage') 
    
    with open(respath / "model", "rb") as filehandler :
        mech = pickle.load(filehandler)
        
    loadfilename = respath / "fields"
    fieldload = liplog.stepArraysFiles(loadfilename)
    
    fig, axes = plt.subplots(2, 3, figsize=(12,7), squeeze = True)
    
    with open(respath/'loadDisplacement.npy', 'rb') as f:
        imposed_disp  = np.load(f)
        reaction_x    = np.load(f)
        
    axcurve = axes[0][0]
    axcurve.set_xlabel(r'Imposed Displacement (mm)')
    axcurve.set_ylabel(r'Reaction Force (N/mm)')
    axcurve.plot(imposed_disp, reaction_x,'-')
    
    axcurve.set_xlim(-0.2,2.2)
    def myplotstep(step, fig, ax):
        logger.info('Plotting Step %d'%step)
        rstep =  fieldload.load(step)
        #ux = incs[step]
        u  = rstep['u']
        d  = rstep['d']
        #stress = mech.stress(u, d)
        cd, figd, axd  = mech.mesh.plotScalarField(d, u*0.,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = ax)
        cbard = fig.colorbar(cd, ax=axd, orientation = 'vertical')
        cbard.set_label(r'$d$')
        axd.set_axis_off()
        axd.axis('equal')
        #axd.set_title(r"$D$") 
        axcurve.plot(imposed_disp[step], reaction_x[step],'o',color='blue')
        
    myplotstep(1, fig, axes[0][1])
    myplotstep(3, fig, axes[0][2])
    myplotstep(5, fig, axes[1][0])
    myplotstep(6, fig, axes[1][1])
    myplotstep(7, fig, axes[1][2])
    
    fig.savefig(respath / "PlateWithHole_Fig_1.png", format = 'png', dpi =400, bbox_inches='tight')
    fig.savefig(respath / "PlateWithHole_Fig_1.pdf", format = 'pdf', bbox_inches='tight')
    

if __name__ == '__main__':
    import sys
    respath =None
    if len(sys.argv )> 1:
        now = datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")
        respath = pathlib.Path(sys.argv[1]) / pathlib.Path(now)
        respath = compute(respath)
    else:
        respath = compute()
    post(respath)
    