#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon APRIL 17 9:54:10 2022

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


#note cohesive model : lm = 2.*Yc*lc/Gc

#Init timing :
#INFO - name       inc time(s)  inc cpu time(s) tot time(s)  tot cpu time(s)
#INFO - Time Loop     7056.13           3118.00     7056.13          3118.00
#INFO - Compute D     6931.83           2993.85     6931.83          2993.85
#INFO - Compute Acc      71.64             71.53       71.64            71.53

#PAtch fix :
#INFO - name       inc time(s)  inc cpu time(s) tot time(s)  tot cpu time(s)
#INFO - Time Loop     5056.73           3478.22     5056.73          3478.22
#INFO - Compute D     4931.19           3352.80     4931.19          3352.80
#INFO - Compute Acc      72.25             72.17       72.25            72.17
#Dissipated energie 3.572430e+04 J

# Serial, my cvxopt.
#INFO - name       inc time(s)  inc cpu time(s) tot time(s)  tot cpu time(s)
#INFO - Time Loop     7763.83          12127.74     7763.83         12127.74
#INFO - Compute D     7633.48          11997.21     7633.48         11997.21
#INFO - Compute Acc      72.52             72.83       72.52            72.83


import sys
#sys.path = ['/home/nchevaug/.local/lib/python3.8/site-packages/cvxopt-1.2.7+0.gd5a21cf.dirty-py3.8-linux-x86_64.egg'] + sys.path
sys.path = ['/home/nchevaug/.local/lib/python3.8/site-packages/cvxopt-0+unknown-py3.8-linux-x86_64.egg']+sys.path
import cvxopt
import os
import matplotlib.pyplot as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from   lip2d.mesh import simplexMesh, dualLipMeshTriangle
import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector
import lip2d.liplog    as liplog
import lip2d.dynamics as dynamics
import shutil
import pathlib

import scipy
import scipy.sparse


def initProblem(logger, timer = None):    
    logger.info('Setting up Kalthoff')
    meshfilename = '../msh/Kalthoff_r2.msh'
    #meshfilename = '../msh/Kalthoff_r0.msh'
    
    mesh = simplexMesh.readGMSH(meshfilename)
    L = 0.1 # lenght of the Domain
    lc = 20.*L/1000. # lenght scaleof the lip constrain (m)
    E = 190e9 #Pa
    nu = 0.3
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    eta = 0.1
    rho = 8000. #kg/m3 
    Gc = 22.2e3  #J/m2
    Yc = Gc/4./lc
    v0 = 16.54 # Impactor initial speed (m/s)
    v0 = 100. # Impactor initial speed (m/s)
    
    massfac = 1. #added mass on boundary = total mass sample*massfac
    
    law = mlaws.SofteningElasticityAsymmetric(lamb, mu, Yc = Yc,
                                            h = mlaws.HQuadratic(), 
                                            g = mlaws.GO4Eta(eta))
    
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
    solverdlipoptions ={'mindeltad':1.e-3, 'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'},
                        'fixpatchbound': False,
                        }
    
    nv = mesh.nvertices
    nf = mesh.ntriangles
    logger.info('Mesh Loaded from file ' + meshfilename)
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    
    lipproj = lipprojector.damageProjector2D(lipmesh)
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
         
    idb = 10#id bottom (fixed in y)
    #idr = 11#id right 
    idlt = 13#id lefttop (fixed in x)
    idimpact =  16 # id of the  impacted right boundary
    
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([idb],{'y':0.})
   # bc.dirichlets.add([idlt],{'x':0.})
    
    # Added Mass on impacted edge
    totalmass = sum(mesh.areas())*rho
    addedmass  = scipy.sparse.dok_matrix((nv,nv))
    vids = mesh.getVerticesOnClassifiedEdges(idimpact) 
    n = len(vids)
    for i in  vids : addedmass[i,i] = totalmass*massfac/n 
    #set dynamical system    
    dynsys = dynamics.lipfieldDynamicSystem(mech, bc, rho, solverdlip, solverdlipoptions, lump = True, addedmass= addedmass, timer =timer)
    # set initial velocity
    u, v, a, d = dynsys.zeros()
    # impose initial speed.
    for i in  vids : v[i,0] = v0
   
    wavespeed = np.sqrt(E/rho) # m/s
    tend = 4.*L/wavespeed #s
    hmin = np.min(mesh.innerCirleRadius())
    dt = 1.*hmin/wavespeed
    t = 0.
    step = 0
    #print(tend, dt, tend/dt)
    #raise
    return dynsys, step, t, tend,dt, u,v,a

def Kplot(mech, u, d, name, dpi = 100, pmin = None, pmax = None):
        plt.close('all')
        mesh = mech.mesh
        strain = mech.strain(u)
        stress = mech.law.trialStress(strain, d)
        p = stress[:,0] + stress[:,1]
        fig = plt.figure()
        axes =    fig.subplots(1,2)
        axd = axes[0]
        axs = axes[1]
        mesh.plotScalarElemField(d, Tmin= 0., Tmax = 1., showmesh = False, fig=fig, ax = axd)
        cexx, ignore, ignore  =mesh.plotScalarElemField(p, u*2, showmesh = False, fig=fig, ax = axs, eraseifFalse =  np.where(d < 0.99)[0], Tmin = pmin, Tmax = pmax)

        for axi in axes.flatten() :
            axi.axis('equal')
            axi.set_axis_off()
        fig.savefig(name, format = 'png', dpi =dpi, bbox_inches='tight')
        
def Kplot_s(mech, u, d, name, dpi = 100, pmin = None, pmax = None):
        plt.close('all')
        mesh = mech.mesh
        strain = mech.strain(u)
        stress = mech.law.trialStress(strain, d)
        p = stress[:,0] + stress[:,1]
        fig = plt.figure()
        axs =    fig.subplots(1,1)
       
       
        #mesh.plotScalarElemField(d, Tmin= 0., Tmax = 1., showmesh = False, fig=fig, ax = axd)
        cexx, ignore, ignore  =mesh.plotScalarElemField(p, u*2, showmesh = False, fig=fig, ax = axs, eraseifFalse =  np.where(d < 0.99)[0], Tmin = pmin, Tmax = pmax)

        #for axi in axes.flatten() :
        axs.axis('equal')
        axs.set_axis_off()
        fig.savefig(name, format = 'png', dpi =dpi, bbox_inches='tight')
        
def post(respath, steps):
    modelfilename = respath/ pathlib.Path('model')
    dynsys = dynamics.lipfieldDynamicSystem.load(modelfilename)
    mech = dynsys
    loadfilename = respath / pathlib.Path("fields")
    fieldload = liplog.stepArraysFiles(loadfilename)
    pmin, pmax  = np.inf, -np.inf
    for step in steps :
        rstep =  fieldload.load(step)
        u  = rstep['u']
        d  = rstep['d']
        strain = mech.strain(u)
        stress = mech.law.trialStress(strain, d)
        p = stress[:,0] + stress[:,1]
        pmin = min(pmin, np.amin(p))
        pmax = max(pmax, np.amax(p))  
    for step in steps :
        print('export step %08d'%step)
        rstep =  fieldload.load(step)
        u  = rstep['u']
        d  = rstep['d']
        Kplot_s(mech, u,d, respath/pathlib.Path("dyn_S_step_%08d.png"%step), dpi = 400, pmin = pmin, pmax = pmax )
    # make movie :    
    #ffmpeg -framerate 5 -pattern_type  glob -i "dyn*.png" output.mp4
        
if __name__ == '__main__': 
#    print(cvxopt)
#    raise
    plt.close('all')
    respath = pathlib.Path.home()/pathlib.Path('tmp/Kalthoff/test_checkcvxopt_para_t')
    os.makedirs(respath, exist_ok = True)
    shutil.copy(__file__, respath / "Run_script.py")
    logger = liplog.setLogger(respath / "Kalthoff.log")  
    logger.info('Starting Program Kalthoff')
    logger.info('All results saved in ' + str(respath))
    timer = liplog.timer()  
    dynsys, step, t, tend ,dt, u,v,a = initProblem(logger, timer)   
    dynamics.lipfieldDynamicSystem.save(dynsys, respath/pathlib.Path('dynmodel') )
    
    saveperiod = 20 # save field every saveperiod steps
    
    fieldsave  = liplog.stepArraysFiles(respath / pathlib.Path("fields"))
    dynsolver  = dynamics.NewmarkExplicit(dynsys) 

    ec = [dynsys.ec(v)]
    ep = [dynsys.ep(u)]
    time = [t]
    fieldsave.save(step,  u = u, v=v, a =a, d = dynsys.d)
    
    logger.info('Starting time iteration. t =%e, tend =%e, dt = %e, nstep = %d'%(t,tend,dt, int((tend-t)/dt)+1 ))
    timer.start("Time Loop")
    while t <= tend :
        logger.info("step %08d, time = %010.8f"%(step,t))
        u,v,a,t = dynsolver.step(u,v,a,t,dt)
        if not  (step%saveperiod):  fieldsave.save(step,  u = u, v=v, a=a, d = dynsys.d)
        ec.append(dynsys.ec(v))
        ep.append(dynsys.ep(u))
        time.append(t)
        step  +=1
    timer.end("Time Loop")
        
    ec = np.array(ec)
    ep = np.array(ep)
    time = np.array(time)
    timer.log(logger)
    
    with open( respath/pathlib.Path('time_energy.npz'),'wb') as f :
        np.savez(f,time = time, ec = ec, ep = ep)
        
    with open(respath/pathlib.Path('time_energy.npz'), 'rb') as f:
        data = np.load(f)
        time, ec, ep  = data['time'], data['ec'], data['ep']
    
    e = ep+ec
    print('Dissipated energie %e J'%(e[0] - e[-1]) )
        
    plt.ion()
    fig = plt.figure()
    plt.plot(time, ec, label ='ec')
    plt.plot(time, ep, label ='ep')
    plt.plot(time, ep+ec, label = 'e')
    plt.legend()
    fig.savefig(respath/pathlib.Path("dyn_D_glob.pdf"), format = 'pdf', bbox_inches='tight')
    

def Redo(respath, startstep, endstep, pr = None):
    
    redo_respath = pathlib.Path.home()/pathlib.Path('tmp/Kalthoff/redo')
    os.makedirs(redo_respath, exist_ok = True)
    logger = liplog.setLogger(redo_respath / "redo_Kalthoff.log")  
    logger.info('Starting Program Redo_Kalthoff')
    
    modelfilename = respath/ pathlib.Path('dynmodel')
    dynsys = dynamics.lipfieldDynamicSystem.load(modelfilename)
    print(dynsys.solverdoptions)
    
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
    solverdlipoptions ={'mindeltad':1.e-3, 'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        #'kktsolveroptions': {'mode':'direct', 'linsolve':'umfpack'}
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        
                        }
    dynsys.solverd = solverdlip
    dynsys.solverdoptions = solverdlipoptions
    
    print(dynsys.solverdoptions)
    
    dynsys.mech.logger = logger
    loadfilename = respath / pathlib.Path("fields")
    fieldload = liplog.stepArraysFiles(loadfilename)
    rstep =  fieldload.load(startstep)
    u  = rstep['u']
    v =  rstep['v']
    a =  rstep['a']
    d  = rstep['d']
    dynsys.dmin = d.copy()
    dynsys.dk = d.copy()
    dynsys.d  = d.copy()
    
    with open(respath/pathlib.Path('time_energy.npz'), 'rb') as f:
        data = np.load(f)
        time, ec, ep  = data['time'], data['ec'], data['ep']
    
    t = time[startstep]
    dt = time[startstep+1] -t
    
    #dynsys.solverdoptions['parallelpatch'] = 

    dynsolver  = dynamics.NewmarkExplicit(dynsys)   
    u,v,a,t = dynsolver.step(u,v,a,t,dt)
    if pr is not None : pr.enable()
    u,v,a,t = dynsolver.step(u,v,a,t,dt)
    #Kplot(dynsys.mech, u,d, redo_respath/pathlib.Path("dyn_D_step_%08d.png"%(step+1)), dpi = 400)
    if pr is not None : pr.disable()
