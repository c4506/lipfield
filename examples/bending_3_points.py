#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021
@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
#note cohesive model : lm = 2.*Yc*lc/Gc

import os
import matplotlib.pylab as plt
import numpy as np
import datetime
import cProfile
import shutil
import sys
sys.path.append('../.')
sys.path.append('../lib')
import material_laws_planestrain
from mesh import simplexMesh, dualLipMeshTriangle
import mechanics2D as mech2d
from mechanics2D import Mechanics2D
import lipdamage as lipprojector
import liplog


### cleaning up previous plots
plt.close('all')

### path to output files and logger
respath = '../tmp/bending_3_points'+str(datetime.datetime.now())
os.makedirs(respath, exist_ok = False)
shutil.copy(__file__, respath+'/Run_script.py')
logger = liplog.setLogger(respath=respath, basefilename = 'bending_3_points')    
   
logger.info('Starting Program 3 points bending')


### mesh file
meshid = str(1)
basemeshname = 'Bending3points_ref'+meshid
meshfilename = '../msh/'+basemeshname+'.msh'

### field output
lc = 10.
savefilename = respath +'/'+ basemeshname+'_lc_'+str(lc)
fieldsave  = liplog.stepArraysFiles(savefilename)

### material properties
E = 30000.
nu = 0.24
Yc = 100.
lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
#law = material_laws_planestrain.SofteningElasticity(lamb, mu, h = material_laws_planestrain.HCohesive(0.4) )
law = material_laws_planestrain.ASSIMSofteningElasticity(lamb, mu, Yc = Yc,
#                                                    h = material_laws_planestrain.HQuadratic(), 
                                                    h = material_laws_planestrain.HCohesive(0.05),
                                                    g = material_laws_planestrain.GO4Eta(0.1))

### name for outputfiles 
expname = basemeshname+'_lc_'+str(lc)

### Displacement Solver
#solverdisp = Mechanics2D.solveDisplacementFixedDLinear
#solverdispoptions = {'linsolve':'cholmod'}
solverdisp = Mechanics2D.solveDisplacementFixedDNonLinear
solverdispoptions = {'linsolve':'cholmod', 'itmax':1000, 'resmax':1.e-6}

### Damage solver
solverd = Mechanics2D.solveDLipBoundPatch
solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                    'lip_reltole':1.e-6,
                    'lipmeasure':'triangle', 'FMSolver':'edge', 
                    'parallelpatch':True, 'snapthreshold':0.999,
                    'kktsolveroptions': {'mode':'direct', 'linsolve':'umfpack'}
                    }


alternedsolveroptions= {'abstole':1.e-8, 'reltole':1.e-5, 'deltadtol':1.e-5}
### start profiling
pr = cProfile.Profile()

### read Mesh
mesh = simplexMesh.readGMSH(meshfilename)
nv = mesh.nvertices
nf =  mesh.ntriangles
logger.info('Mesh Loaded from file ' + meshfilename)
logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nv))
### build lipMesh
lipmesh = dualLipMeshTriangle(mesh)
logger.info('LipMesh constructed from Mesh')
logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
### damage projector object
lipproj = lipprojector.damageProjector2D(lipmesh)
### Mechanical Problem
mech = Mechanics2D(mesh, law, lipproj, lc, log=logger)       
### increment
restart_path = None
#restart_path = '/home/chevaugeon/Recherche/Softening/lipshitz/Lipchitz_softening_2d/Bending3points_ref3_lc_20.0_u_260.0_.npy'
increment = np.linspace(0.,300.,61)
### Boundary Conditions     
bc = mech2d.boundaryConditions()
bc.dirichlets = mech2d.dirichletConditions(mesh)
bc.dirichlets.add([20],{'y':0.})
bc.dirichlets.addPoints([1], {'x':0., 'y':0.})
did = bc.dirichlets.add([10],{'y':0.}) 
### ploting options
onlineplot = True
showmesh = nv < 1000        


### initial value for u and d
u, d = mech.zeros()

if restart_path is not None :
    with open(restart_path,'rb') as f:
        u = np.load(f)
        d = np.load(f)

### load displacement curve data initialization
uimp=[]
Fx  =[]
fig0, ax0 = plt.subplots(1,1)
fig0.suptitle(r'$F(u)$')
ax0.set_xlabel(r'Imposed Displacement')
ax0.set_ylabel(r'Reaction Force')
### Loop on load increment
#for ui in np.linspace(0.,66.,20) :

### global timing info
timer = liplog.timer() 

for ui in increment :
    timer.new_increment()
    timer.start('total')
    logger.info('#Start uimp = '+ str(ui))
    ### set imposed displacement
    bc.dirichlets.update(did, {'y':-ui})
    dmin = d.copy() 
    pr.enable()

    
    res = mech.alternedDispDSolver(dmin = dmin, dguess= d.copy(), un=u, bc=bc, 
                                   alternedsolveroptions = alternedsolveroptions,
                                   solverdisp = solverdisp,
                                   solverdispoptions= solverdispoptions,
                                   solverd = solverd,
                                   solverdoptions = solverdlipoptions,
                                   timer = timer)
    if not res['Converged'] :
        print ('alterned Solver did not converge at step ui =', ui, ' message is ', res['info'])
        u = res['u']
        d = res['d']
        mech.plots(u, d, ui, respath+'/'+expname+'LastBeforeFailure', showmesh = showmesh) 
        break
    
    dtmp1 = res['d']
    dmind1 = np.linalg.norm(dtmp1-dmin)
    logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dmin-d1|= ' + '%2.e'%dmind1)
    pr.disable()
    u = res['u']
    d = res['d']
    R = res['R']
    ### updating load displacement curve
    Rx13 = -sum(bc.dirichlets.getReactions(did, R))
    
    uimp.append(ui)
    Fx.append(Rx13)
    ax0.plot(np.array(uimp), np.array(Fx), '-o')
    ### save u, d for the increment
    with open(respath+'/'+expname+'_u_'+str(ui)+'_.npy', 'wb') as f:
        np.save(f,u)
        np.save(f,d)
    ### plotting the field
    if onlineplot :     mech.plots(u, d, ui, respath+'/'+expname, showmesh = showmesh) 
    
    timer.end('total')
    timer.log(logger)

### End of Increments

### Save load disp points
uimp = np.array(uimp)
f =  open(respath+'/'+expname+'_RU_.npy', 'wb')
np.save(f,uimp)
np.save(f,Fx)
f.close()
### Plot load disp curve
fig, ax = plt.subplots(1,1)
fig.suptitle(r'$F(u)$')
plt.plot(uimp, np.array(Fx), '-o')
ax.set_xlabel(r'Imposed Displacement')
ax.set_ylabel(r'Reaction Force')
fig.savefig(respath+'/'+expname+'Load_Displacement_.pdf', format = 'pdf')    
