#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
// Copyright (C) 2021 Chevaugeon Nicolas
Created on Mon Jan 25 13:54:10 2021

@author: chevaugeon

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""



import os
import matplotlib.pylab as plt
import numpy as np
import lip2d.material_laws_planestrain as mlaws
from lip2d.mesh import simplexMesh, dualLipMeshTriangle

import lip2d.mechanics2D as mech2d
import lip2d.lipdamage as lipprojector
import lip2d.liplog as liplog
#import datetime
import shutil
import pathlib
import pickle


def compute(assymlaw=True, respathbase = pathlib.Path.home()/pathlib.Path('tmp/resultsShearTest')):
    onlineplot = True
    lc = 0.015 # lenght scaleof the lip constrain
    Gc = 2.7 # 2700 J/m2 -> 2.7 Mpa.mm
    E = 210000 #MPa
    nu = 0.2
    lamb, mu = (E*nu/(1.+nu)/(1.-2*nu), E/2./(1+nu))
    Yc = Gc/4./lc
    eta = 0.1
    
    if assymlaw :
        basemeshname = 'shear_test_assym'
        resdir = pathlib.Path('Asym')
        law = mlaws.SofteningElasticityAsymmetric(lamb, mu, Yc,
                                                        h = mlaws.HQuadratic(), 
                                                        g = mlaws.GO4Eta(eta))
        # Solver for equilibrium, need nonlinear solver (newton)
        solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDNonLinear
        solverdispoptions =  {'linsolve':'cholmod', 'itmax':20, 'maxnormabs':1.e-8, 'maxnormrel':1.e-6}
        umax = 0.03
    else :
        basemeshname = 'shear_test_v4'
        #basemeshname = 'shear_test_v2_coarse'
        resdir = pathlib.Path('Sym')
        # Define the material law 
        law = mlaws.SofteningElasticitySymmetric(lamb, mu, Yc,
                                                        h = mlaws.HQuadratic(), 
                                                        g = mlaws.GO4Eta(eta))
        # Solver for equilibrium
        solverdisp = mech2d.Mechanics2D.solveDisplacementFixedDLinear
        solverdispoptions = {'linsolve':'cholmod'}
        umax = 0.05
        
    incs = np.linspace(0.,umax,401)
    #incs = np.linspace(0.,umax/10,40)
  
    respath = respathbase/resdir #+ datetime.datetime.now().strftime("%Y_%m_%d_%HH_%MM_%SS")
    os.makedirs(respath, exist_ok = False)
    shutil.copy(__file__, respath/'Run_script.py')
    logger = liplog.setLogger(respath/'ShearTest.log')     
    
    savefilepath =  respath/ 'fields'
    fieldsave  = liplog.stepArraysFiles(savefilepath)
        
    logger.info('Starting Program shear_test, results saved in '+ str(respath.absolute()))
    
    plt.close('all')
    
    
    solverdlip = mech2d.Mechanics2D.solveDLipBoundPatch 
#    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
#                        'lip_reltole':1.e-6,
#                         'lipmeasure':'triangle', 'FMSolver':'triangle', 
#                         'parallelpatch':True, 'snapthreshold':0.999,
#                         'kktsolveroptions': {'mode':'direct', 'linsolve':'umfpack'}
#                         }
    
    solverdlipoptions ={'mindeltad':1.e-3, 'fixpatchbound':False, 
                        'lip_reltole':1.e-6,
                        'lipmeasure':'triangle', 'FMSolver':'edge', 
                        'parallelpatch':False, 'snapthreshold':0.999,
                        'kktsolveroptions': {'mode':'schur', 'linsolve':'cholmod'}
                        }
    alternedsolveroptions= {'abstole':1.e-8, 'reltole':1.e-5,  'deltadtol':1.e-4, 'outputalliter':True, 'verbose':False, 'stoponfailure':False}
    
    meshfilepath = pathlib.Path('../msh/'+basemeshname+'.msh')
    
    mesh = simplexMesh.readGMSH(meshfilepath)
    nv = mesh.nvertices
    nf =  mesh.ntriangles
    logger.info('Mesh Loaded from file ' + str(meshfilepath.absolute()))
    logger.info('Mesh size : nv: '+ str(nv) + ' nf: '+ str(nf))
    
    
    lipmesh = dualLipMeshTriangle(mesh)
    logger.info('LipMesh constructed from Mesh')
    logger.info('LipMesh size : nv: '+ str(lipmesh.nvertices)+' nf: '+ str(lipmesh.ntriangles))
    
    lipproj = lipprojector.damageProjector2D(lipmesh)
    
    mech = mech2d.Mechanics2D(mesh, law, lipproj, lc, log = logger)       
      
    
    fixedlineid = 100#id of fixed line
    pulledlineid = 101#id of pulled line
    
    bc = mech2d.boundaryConditions()
    bc.dirichlets = mech2d.dirichletConditions(mesh)
    bc.dirichlets.add([fixedlineid],{'x':0., 'y':0.})
    bc.dirichlets.add([pulledlineid],{'y':0.})
    did = bc.dirichlets.add([pulledlineid],{'x':0.})
        
    u, d= mech.zeros()

        
    uimp=[]
    Fx  =[]
    etab= []
    showmesh = nv < 1000
    
    with open(respath / "model", "wb") as filehandler :
        pickle.dump(mech,filehandler)

    ### global timing info
    timer = liplog.timer() 
    for step, ui in enumerate(incs) : 
        logger.info('#Start uimp = '+ str(ui))
        timer.new_increment()
        timer.start('total')     
        bc.dirichlets.update(did,{'x':ui})
        dn = d.copy() 
        res = mech.alternedDispDSolver(dmin = dn, dguess= d.copy(), un=u, 
                                       bc=bc, 
                                       incstr='Step %04d'%step,
                                       alternedsolveroptions = alternedsolveroptions,
                                       solverdisp = solverdisp,
                                       solverdispoptions = solverdispoptions,
                                       solverd = solverdlip,
                                       solverdoptions = solverdlipoptions,
                                       timer =timer)
        u, d, R = res['u'], res['d'], res['R']
        fieldsave.save(step,  u = u, d = d, R = R) 
        logger.info(' End uimpminimize = '+ str(ui) + ' Conv :' + str(res['iter']) + ' iter, |dn-d|= ' + '%2.e'%np.linalg.norm(d-dn))
        if onlineplot :     mech.plots(u, d, ui, respath/'plot', fields =np.array([['d_1','stressxy' ],['Y', 'd']]),  showmesh = showmesh) 
        if not (res['Converged']) : raise
        
        strain =mech.strain(u)
        e = mech.energy(strain, d)   
        Rx = sum(bc.dirichlets.getReactions(did, R))
        uimp.append(ui)
        etab.append(e)
        Fx.append(Rx)
        
        timer.end('total')
        timer.log(logger)
       

    f =  open(respath/'RU.npy', 'wb')
    np.save(f,np.array(uimp))
    np.save(f,np.array(Fx))
    f.close()
    
    fig, ax = plt.subplots(1,1)
    fig.suptitle(r'$F(u)$')
    plt.plot(uimp, np.array(Fx), '-o')
    ax.set_xlabel(r'Imposed Displacement')
    ax.set_ylabel(r'Reaction Force')
    fig.savefig(respath/'Load_Displacement.pdf', format = 'pdf')    
    
def post(respath):
    loadfilename = path+'/'+ basemeshname+'_lc_'+str(lc)
    fieldload = liplog.stepArraysFiles(loadfilename)
    uscale = 0.01
    utab = []
    Ftab = []
    
    for step in dopost['steps']:
        fig, axes = plt.subplots(1, 2, figsize=(18,7), squeeze = True)
        logger.info('Plotting Step %d'%step)
        rstep =  fieldload.load(step)
        ux = incs[step]
        u  = rstep['u']
        d  = rstep['d']
        stress = mech.stress(u, d)
        
        cs, figs, axs  = mesh.plotScalarField(stress[:,2], u*uscale, Tmin = -1200, Tmax = 1200, showmesh=False, fig = fig, ax = axes[0])
        cd, figd, axd  = mesh.plotScalarField(d, u*uscale,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = axes[1])
        cbard = fig.colorbar(cd, ax=axd, orientation = 'vertical')
        cbard.set_label('r$D$')
        axd.set_axis_off()
        axd.axis('equal')
        axd.set_title(r"$D$") 
            
        cbars = fig.colorbar(cs, ax=axs, orientation = 'vertical')
        cbars.set_label('r$\sigma_{xy}$, Mpa')
        
        axs.set_axis_off()
        axs.axis('equal')
        axs.set_title(r"$\sigma_{xy}$") 
        fig.suptitle('Loadingfactor : %.4f'%ux)       
        fig.tight_layout()
        fig.savefig(respath+'/'+basemeshname+'_lc_'+str(lc)+'_u_%.4f'%ux+'.'+'png', format='png') 
        
        #plt.close('all')
        R = rstep['R']
        Rx = sum(bc.dirichlets.getReactions(did, R))
        pulledlineid
        utab.append(ux)
        Ftab.append(Rx)
    
    fig, ax = plt.subplots(1,1)
    fig.suptitle(r'$F(u)$')
    ax.set_xlabel(r'Imposed Displacement')
    ax.set_ylabel(r'Reaction Force')
    plt.plot(utab, Ftab,'-o')
    fig.savefig(savefilename+'_Load_Displacement.pdf', format = 'pdf')    


    
    if dofigpaper is not None:
        path = dofigpaper['path']
        loadfilename = path+'/'+ basemeshname+'_lc_'+str(lc)
        fieldload = liplog.stepArraysFiles(loadfilename)
        uscale = 0.01
        utab = []
        Ftab = []
        fig, axes = plt.subplots(2, 3, figsize=(12,7), squeeze = True)
        
        
        for step in dofigpaper['steps']:
            logger.info('Plotting Step %d'%step)
            rstep =  fieldload.load(step)
            ux = incs[step]
            R = rstep['R']
            Rx = sum(bc.dirichlets.getReactions(did, R))
            utab.append(ux)
            Ftab.append(Rx)
        
        axcurve = axes[0][0]
        axcurve.set_xlabel(r'Imposed Displacement (mm)')
        axcurve.set_ylabel(r'Reaction Force (N/mm)')
        axcurve.plot(utab, Ftab,'-')
        
        
        def myplotstep(step, fig, ax):
            logger.info('Plotting Step %d'%step)
            rstep =  fieldload.load(step)
            #ux = incs[step]
            u  = rstep['u']
            d  = rstep['d']
            #stress = mech.stress(u, d)
            cd, figd, axd  = mesh.plotScalarField(d, u*0.,Tmin = 0., Tmax =1, showmesh=False, fig = fig, ax = ax)
            cbard = fig.colorbar(cd, ax=axd, orientation = 'vertical')
            cbard.set_label(r'$D$')
            axd.set_axis_off()
            axd.axis('equal')
            #axd.set_title(r"$D$") 
            axcurve.plot(utab[step], Ftab[step],'o',color='blue')
            
        myplotstep(dofigpaper['stepsfield'][0], fig, axes[0][1])
        myplotstep(dofigpaper['stepsfield'][1], fig, axes[0][2])
        myplotstep(dofigpaper['stepsfield'][2], fig, axes[1][0])
        myplotstep(dofigpaper['stepsfield'][3], fig, axes[1][1])
        myplotstep(dofigpaper['stepsfield'][4], fig, axes[1][2])
        fig.tight_layout
        
        fig.savefig(savefilename+'_fig.png', format = 'png', dpi =400, bbox_inches='tight')
        #fig.savefig(savefilename+'_fig.pdf', format = 'pdf', bbox_inches='tight')
        #fig.savefig(savefilename+'_fig.svg', format = 'svg', bbox_inches='tight')
    
    
if __name__ == '__main__':
    import sys
    respath =None
    asym = False
    if len(sys.argv )> 1:
       # now = datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")
        if   sys.argv[1] == "asym" : asym =True
        elif sys.argv[1] == "sym"  : sym = False
    
    if len(sys.argv )> 2:
       # now = datetime.datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss%fms")
        respathbase = pathlib.Path(sys.argv[2]) 
        respath = compute(asym, respath)
    else:
        respath = compute(asym)
    #post(respath)
