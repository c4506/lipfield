### Lipfield 

This software is developed at Ecole Centrale Nantes
by N. Chevaugeon from the GeM Laboratory UMR6183.
it is a complement to the paper :  ["Lipschitz regularization for fracture: the Lip-field approach"
Co-written by N. Moës and N. Chevaugeon](https://arxiv.org/abs/2111.04771)

-prerequist :

python3, numpy, matplotlib, scipy, triangle, cvxopt, sortedcontainers

install the library :
pip install -e ./