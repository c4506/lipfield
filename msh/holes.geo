//+
//ref0
//d0= 0.5;
//d=0.2;
//ref1
//d0= 0.125;
//d=0.125;
//ref2
//d0= 0.0625;
//d=0.0625;
//ref3
#d0= 0.03125;
#d=0.03125;
//ref4
d0= 0.015625;
d= 0.015625;


R = 0.2;
L = 1.;
C1x = -0.4;
C1y = -0.4;
C2x = +0.4;
C2y = +0.4;

Point(1) = {C1x, C1y, 0., d};
Point(2) = {-L, -L, 0., d0};
Point(3) = {L, -L, 0., d0};
Point(4) = {L, L, 0., d0};
Point(5) = {-L, L, 0., d0};

Point(6) = {C1x-R, C1y, 0., d};
Point(7) = {C1x+R, C1y, 0., d};
Point(8) = {C1x, C1y-R, 0., d};
Point(9) = {C1x, C1y+R, 0., d};

Point(11) = {C2x, C2y, 0., d};
Point(16) = {C2x-R, C2y, 0., d};
Point(17) = {C2x+R, C2y, 0., d};
Point(18) = {C2x, C2y-R, 0., d};
Point(19) = {C2x, C2y+R, 0., d};

//+
Line(1) = {2, 3};
//+
Line(2) = {3, 4}; //right
//+
Line(3) = {4, 5};
//+
Line(4) = {5, 2}; //left
//+


//+
Circle(5) = {7, 1, 9};
Circle(6) = {9, 1, 6};
Circle(7) = {6, 1, 8};
Circle(8) = {8, 1, 7};

Circle(15) = {17, 11, 19};
Circle(16) = {19, 11, 16};
Circle(17) = {16, 11, 18};
Circle(18) = {18, 11, 17};

//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Curve Loop(2) = {6, 7, 8, 5};
Curve Loop(3) = {16, 17, 18, 15};

//+
Plane Surface(1) = {1, 2, 3};
Physical Point(2) = {2};
Physical Point(3) = {3};
Physical Point(4) = {4};
Physical Point(5) = {5};
//+
Physical Curve(10) = {4}; //left
//+
Physical Curve(11) = {3};
//+
Physical Curve(12) = {2}; //right
//+
Physical Curve(13) = {1};
//+
Physical Curve(14) =  {7,  6,  5,  8};
Physical Curve(15) = {17, 16, 15, 18};

//+
Physical Surface(100) = {1};
