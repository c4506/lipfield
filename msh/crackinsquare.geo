//+
//ref0
//d0= 0.5;
//d=0.2;
//ref1
//d0= 0.125;
//d=0.125;
//ref2
//d0= 0.0625;
//d=0.0625;
//ref3
d0= 0.03125;
//ref4
//d0= 0.015625;
//d= 0.015625;
delta = 0.;

L = 1.;

Point(1) = {-L, -L, 0., d0};
Point(2) = {L, -L, 0., d0};
Point(3) = {-L, -delta/2., 0., d0};
Point(4) = {L, -delta/2., 0., d0};
Point(5) = {-L, delta/2., 0., d0};
Point(6) = {L, delta/2., 0., d0};
Point(7) = {L, L, 0., d0};
Point(8) = {-L, L, 0., d0};


//+
Line(1) = {1, 2}; // bot
Line(2) = {2, 4}; //rightbot 
Line(3) = {4, 3}; //mid  bot (crack)
Line(4) = {3, 1}; //left bot

Line(5) = {6, 5}; // mid  top (crack)
Line(6) = {5, 8}; //  left top 
Line(7) = {8, 7}; //  top 
Line(8) = {7, 6}; //l right top  
 
 
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {8, 7, 6,5};
//+
Plane Surface(2) = {2};

//+

Physical Point("Pbotleft",       10) = {1};
Physical Point("Pbotright",      11) = {2};
Physical Point("Pbotcrackleft",  12) = {3};
Physical Point("Pbotcrackright", 13) = {4};

Physical Point("Ptopleft",       20) = {8};
Physical Point("Ptopright",      21) = {7};
Physical Point("Ptopcrackleft",  22) = {5};
Physical Point("Ptopcrackright", 23) = {6};

Physical Line("Lbotcrack",      100) = {3};
Physical Line("Lbotleft",       101) = {4};
Physical Line("Lbotright",      102) = {2};
Physical Line("Lbotbot",        103) = {1};

Physical Line("Ltopcrack",      200) = {5};
Physical Line("Ltopleft",       201) = {6};
Physical Line("Ltopright",      202) = {8};
Physical Line("Ltoptop",        203) = {7};

//+
Physical Surface("Sbot", 1000) = {1};
Physical Surface("Stop", 2000) = {2};
