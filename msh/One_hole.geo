//Mesh Size
h = 0.1;

L 	=  5 ; //Dimension de l'arrete de la plaque carré 
D   =  1 ; //Diametre du trou
R 	=  D/2; //Rayon du trous 

//Coordonnés du centre du trou
Cx 	= -0. ;
Cy	= -0. ;


Point(1) = { Cx  , Cy		, 0., 	h };

Point(2) = {-L	 ,-L		, 0.,	h };
Point(3) = { L	 ,-L		, 0.,	h };
Point(4) = { L	 , L		, 0.,	h };
Point(5) = {-L	 , L		, 0.,	h };

Point(6) = { Cx-R, Cy		, 0.,	h };
Point(7) = { Cx+R, Cy		, 0.,	h };
Point(8) = { Cx	 , Cy-R		, 0.,	h };
Point(9) = { Cx  , Cy+R		, 0.,	h };


Line(1) = {2, 3};
Line(2) = {3, 4}; //right
Line(3) = {4, 5};
Line(4) = {5, 2}; //left


Circle(5) = {7, 1, 9};
Circle(6) = {9, 1, 6};
Circle(7) = {6, 1, 8};
Circle(8) = {8, 1, 7};


Curve Loop(1) = {4, 1, 2, 3};
Curve Loop(2) = {6, 7, 8, 5};

Plane Surface(1) = {1, 2};


Physical Curve(10) = {4};	//left
Physical Curve(20) = {3};	//top
Physical Curve(30) = {2};	//right
Physical Curve(40) = {1};	//bottom
Physical Curve(50) = {5,6,7,8};	//hole

Physical Surface(100) = {1};

