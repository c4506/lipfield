//+
//l  = 0.125; //ref3
//l  = 0.0625; //ref4
L =  500.; // lenght of the beam
D =  400.; // Distance between bottom points
H  = 100.; // height of the beam
L0 = 60.; // lenght on whitch the load is applied
// ref0 h = 10.
// ref1 h =  5.
// ref2 h =  2.5
// ref3 h =  1.25

h  = 1.25  ; 
Point(1) = {      -L/2., 0., 0., h};
Point(2)  = {-D/2.-L0/2., 0., 0., h};
Point(4)  = {-D/2.+L0/2., 0., 0., h};
Point(6)  = { 0.,     0., 0., h};
Point(7)  = { D/2.+L0/2., 0., 0., h};
Point(9)  = { D/2.-L0/2., 0., 0., h};
Point(10) = { L/2.,   0., 0., h};

Point(11)  = {   L/2., H, 0., h};
Point(12)  = {   D/2., H, 0., h};
Point(13)  = {  L0/2., H, 0., h};
Point(14)  = {     0., H, 0., h};
Point(15)  = { -L0/2., H, 0., h};
Point(16)  = {  -D/2., H, 0., h};
Point(17) =  {  -L/2., H, 0., h};

//+
Line(1) = {1, 2};
//+
Line(2) = {2, 4};
//+
Line(3) = {4, 6};
//+
Line(5) = {6, 9};
//+
Line(6) = {9, 7};
//+
Line(7) = {7, 10};
//+
Line(8) = {10, 11};
//+
Line(9) = {11, 12};
//+
Line(10) = {12, 13};
//+
Line(11) = {13, 14};
//+
Line(12) = {14, 15};
//+
Line(13) = {15, 16};
//+
Line(14) = {16, 17};
//+
Line(15) = {17, 1};
//+
Curve Loop(1) = {3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1, 2};
//+
Plane Surface(1) = {1};
//+
//+
Physical Point(1) = {14};
//+
Physical Curve(20) = {2, 6};
//+
Physical Curve(10) = {11, 12};
Physical Curve(50) = {3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1, 2};
Physical Surface(100) = {1};
