//+
//ref0

N=16;
R1 = 100;
R2 = 150;
h1 = (R2-R1)/N;
h2 = h1;



Point(100) = {0., 0., 0., h1}; // Center

Point(1) = {R1, 0., 0.,  h1};
Point(2) = {0, R1, 0.,  h1};
Point(3) = {-R1, 0., 0., h1};
Point(4) = {0., -R1, 0., h1};

Point(5) = {R2, 0., 0.,  h2};
Point(6) = {0, R2, 0.,   h2};
Point(7) = {-R2, 0., 0., h2};
Point(8) = {0., -R2, 0., h2};


Circle(1) = {1, 100, 2};
Circle(2) = {2, 100, 3};
Circle(3) = {3, 100, 4};
Circle(4) = {4, 100, 1};
Circle(5) = {5, 100, 6};
Circle(6) = {6, 100, 7};
Circle(7) = {7, 100, 8};
Circle(8) = {8, 100, 5};
Curve Loop(1) = {8, 5, 6, 7};
Curve Loop(2) = {4, 1, 2, 3};
Plane Surface(1) = {1, 2};
Physical Surface(100) = {1};
Physical Curve(10) = {1,2,3,4}; // inner circle
Physical Curve(20) = {5,6,7,8}; //outer circle
