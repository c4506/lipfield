//Mesh Size
h = 0.1; //r0
h = 0.1/2.; //r1
h = 0.1/4.; //r2
//h = 0.1/8; //r3
//h = 0.1/16; //r4

L 	=  1 ; // Largeur carre
a   =  0.2 ; // longueur fissure 0.2
cy   =  0.05; //décallage fissure parrapport à l'axe horizonal
delta = 0.001; // 'ouverture'

//Coordonnés du centre du trou




Point(1) = {0	 , 0		, 0.,	h };
Point(2) = {L	 , 0		, 0.,	h };
Point(3) = {L	 , L		, 0.,	h };
Point(4) = {0	 , L		, 0.,	h };

Point(5) = { 0, L/2.-cy		, 0.,	h };
Point(6) = { a, L/2-cy		, 0.,	h/8 };
Point(7) = { 0, L/2-cy+delta		, 0.,	h};
Point(8) = { L	 ,L/2 +cy-delta		, 0., h};
Point(9) = { L-a  ,L/2 +cy		, 0.,	h/8 };
Point(10) = { L  , L/2+cy		, 0.,	h };


Point(11) = {a+4*h*cy	 , L/2.-2*cy		, 0.,	h/8 };
Point(12) = {L-a-4*h*cy	 , L/2.-2*cy		, 0.,	h/8 };
Point(13) = {L-a-4*h*cy	 , L/2. + 2*cy		, 0.,	  h/8 };
Point(14) = {a+4*h*cy	 	, L/2.+2*cy, 0., 	h/8 };

Line(1) = {1, 2};
Line(2) = {2, 8};
Line(3) = {8, 9};
Line(4) = {9, 10};
Line(5) = {10, 3};
Line(6) = {3, 4};
Line(7) = {4, 7};
Line(8) = {7, 6};
Line(9) = {6, 5};
Line(10) = {5, 1};

Line(11) = {11,12};
Line(12) = {12,13};
Line(13) = {13,14};
Line(14) = {14,11};

Curve Loop(1) = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
Plane Surface(1) = {1};
Line {11} In Surface {1};
Line {12} In Surface {1};
Line {13} In Surface {1};
Line {14} In Surface {1};
Physical Point(1)   = {1};
Physical Curve(100) = {1};	//bottom
Physical Curve(101) = {6};	//top
Physical Curve(102) =  { 2, 3, 4, 5, 7, 8, 9, 10};

Physical Surface(100) = {1};
