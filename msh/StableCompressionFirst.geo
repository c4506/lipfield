d0 = 1./8.;

L = 10.;
H= 5.;
R = 1.;
Xc = 8.;
Yc = H/2.;
h = 0.1;
l = 0.5;



Point(1) = {0.,0.,0.,d0};
Point(2) = {L,0.,0., d0};
Point(3) = {L,H,0.,d0};
Point(4) = {0.,H,0.,d0};
Point(5) = {Xc,Yc,0.,d0};
Point(6) = {Xc - Sqrt(R*R-h*h/4), Yc-h/2., 0., d0};
Point(7) = {Xc +R, Yc, 0., d0};
Point(8) = {Xc - Sqrt(R*R-h*h/4), Yc+h/2., 0., d0};
Point(9) = {Xc-R-l, Yc,0., d0};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Circle(5) = {8,5,7};
Circle(6) = {7,5,6};
Line(7) ={6,9};
Line(8) = {9,8};
Curve Loop(1) = {1, 2, 3, 4};
Curve Loop(2) = {6, 7, 8, 5};
Plane Surface(1) = {1, 2};

//bottom left corner
Physical Point(1) = {1};
// left line
Physical Curve(2) = {4};
// right line
Physical Curve(3) = {2};
// all the boundary (needed to construct lip mesh)
Physical Curve(4) = {1, 3, 5, 6, 7, 8};
// all the triangle of the mesh 
Physical Surface(5) = {1};


