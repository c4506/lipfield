//+
d= 0.5;// ref00
//d = 0.1; //ref0
// d = 0.05; //ref1
// d  = 0.025;// ref2
// d = 0.0125; // ref3

L  = 1.;
H = 0.5;
Point(1) = {0., 0., 0., d};
Point(2) = {-L, 0., 0., d};
Point(3) = {-L, H, 0., d};
Point(4) = {H, H, 0., d};
Point(5) = {H, -L, 0., d};
Point(6) = {0., -L, 0., d};



//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};
//+
Curve Loop(1) = {1, 2, 3, 4, 5, 6};
//+
Plane Surface(1) = {1};
//+
Physical Curve(10) = {2};
//+
Physical Curve(11) = {3};
//+
Physical Curve(12) = {4};
//+
Physical Curve(13) = {5};
//+
Physical Curve(14) = {6};
//+
Physical Curve(15) = {1};
//+
Physical Surface(100) = {1};
