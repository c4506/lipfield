//+
//d  = 0.125; //ref3
//d  = 0.0625; //ref1
//d = 0.03125; //ref5
L  = 1.;
H = 0.05;
d = H/5.;
Point(1) = {-L, -H, 0., d};
Point(2) = {L, -H, 0., d};
Point(3) = { L, H, 0., d};
Point(4) = {-L, H, 0., d};



//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Curve(10) = {1};
//+
Physical Curve(11) = {2};
//+
Physical Curve(12) = {3};
//+
Physical Curve(13) = {4};
//+
Physical Surface(100) = {1};
