d0 = 1./8.;
d1 = d0/2.;

L = 10.;
H= 5.;
R = 0.2;
Xc = L/2.;
Yc = H/2.;
h = 0.02;
l = 1.;


Point(1) = {0.,H/2.,0.,d1};
Point(2) = {0.,0.,0.,d0};
Point(3) = {L,0.,0., d0};
Point(4) = {L,H/2.,0., d0};
Point(5) = {L,H,0.,d0};
Point(6) = {0.,H,0.,d0};

Point(7) = {Xc,Yc,0.,d0};
Point(8) = {Xc - Sqrt(R*R-h*h/4), Yc-h/2., 0., d1};
Point(9) = {Xc +R, Yc, 0., d0};
Point(10) = {Xc - Sqrt(R*R-h*h/4), Yc+h/2., 0., d1};
Point(11) = {Xc-R-l, Yc,0., d1};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,1};
Circle(7) = {8,7,9};
Circle(8) = {9,7,10};
Line(9) ={10,11};
Line(10) = {11,8};
Line(11) = {1,11};
Line(12) = {9,4};

Curve Loop(1) = {5, 6, 11, -9, -8, 12, 4};
Plane Surface(1) = {1};
Curve Loop(2) = {11, 10, 7, 12, -3, -2, -1};
Plane Surface(2) = {2};
Physical Point(1) = {4};
Physical Curve(2) = {6, 1};
Physical Curve(3) = {4, 3};
Physical Curve(4) = {5, 2, 10, 7, 8, 9};
Physical Surface(5) = {1, 2};