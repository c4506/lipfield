//Mesh Size
h0 = 0.5; //r0
h0 = 0.5/2.; //r1
h0 = 0.5/4.; //r2
h1 = h0/32;
L 	=  1 ; // Largeur carre
a   =  0.5 ; // longueur fissure 0.2
cy   =  0.05; //décallage fissure parrapport à l'axe horizonal
delta = 0.001; // 'ouverture'

//Coordonnés du centre du trou




Point(1) = {0	 , 0		, 0.,	h0 };
Point(2) = {L	 , 0		, 0.,	h1 };
Point(3) = {L	 , L		, 0.,	h1 };
Point(4) = {0	 , L		, 0.,	h0 };

Point(5) = { 0, L/2.-delta	, 0.,	h0 };
Point(6) = { a, L/2		, 0.,	h1 };
Point(7) = { 0, L/2+delta		, 0.,	h0};

Point(8) = {L/2 ,  L/4		, 0.,	h1 };
Point(9) = {L/2	 , 3*L/4	, 0.,	  h1 };
Point(10) = {L	 , L/2.	, 0.,	 h0};
Point(11) = {L	 , L/4.	, 0.,	 h1};
Point(12) = {L	 , 3*L/4.	, 0.,	 h1};


Line(1) = {1, 2};
Line(3) = {2, 11};
Line(4) = {11, 10};
Line(5) = {10, 12};
Line(6) = {12, 3};
Line(7) = {3, 4};
Line(9) = {4, 7};
Line(10) = {7, 6};
Line(11) = {6, 5};
Line(12) = {5, 1};


Line(13) = {8, 6};
Line(14) = {6, 9};
Line(15) = {9, 3};
Line(16) = {8, 2};
Line(17) = {6, 12};
Line(18) = {6, 11};

Curve Loop(1) = { 1, 3, 4, 5, 6, 7, 9,10,11,12};
Plane Surface(1) = {1};
Line {13} In Surface {1};
Line {14} In Surface {1};
Line {15} In Surface {1};
Line {16} In Surface {1};
Line {17} In Surface {1};
Line {18} In Surface {1};

Physical Point(1)   = {1};
Physical Curve(100) = {1,2};	//bottom
Physical Curve(101) = {7,8};	//top
Physical Curve(102) =  {  3,4,5,  6,  9, 10,11,12 };

Physical Surface(100) = {1};
